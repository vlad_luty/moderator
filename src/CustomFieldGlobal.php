<?php


interface CustomFieldGlobal
{
    const STORYPOINT = 'customfield_10025';
//    const STORYPOINT = 'customfield_10016';
    //const STORYPOINT = 'customfield_10102';

    const TODO_STATUS = 'To Do';
    const DONE_STATUS = 'Done';
    const PROGRESS_STATUS = 'In Progress';

    const WORKING_DAY_IN_SECONDS  = 28800;
}