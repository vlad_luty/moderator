<?php
namespace telegram;


use logics\DbConnection;
use ParagonIE\EasyDB\EasyDB;
use traits\Overloadable;

class SessionData implements \ArrayAccess
{
    use Overloadable;

    const TABLE = 'session';

    private $global = array();

    private $local = array();

    private $data = array();

    /**
     * @var DbConnection
     */
    private $db;

    private $channel_id;

    public function __construct($channel_id)
    {
        $this->channel_id = $channel_id;
        $this->db = DbConnection::getInstance();

        $this->global = $this->init();
        $this->data   = $this->global;
    }

    public function init()
    {
        $session = $this->db
            ->query("SELECT `data` FROM `". self::TABLE ."` WHERE `chid` = :chid AND `bot_id`=:bot")
            ->bind(':chid', $this->channel_id)
            ->bind(":bot", $_REQUEST['bot'])
            ->one();
        if (empty($session)) {
            $sql = "INSERT INTO `". self::TABLE ."` (`bot_id`,`chid`, `dat_add`) VALUES (:bot, :chid, :dat_add)";
        } else {
            $sql = "UPDATE `". self::TABLE ."` SET `dat_add`=:dat_add WHERE `chid` = :chid AND `bot_id`=:bot";
        }

        $this->db
            ->query($sql)
                ->bind(':chid', $this->channel_id)
                ->bind(":bot", $_REQUEST['bot'])
                ->bind(':dat_add', date("Y-m-d H:i:s"))
            ->execute();

        return $this->getGlobal();
    }

    public function getGlobal()
    {
        if (!$this->global) {
            $session = $this->db
                ->query("SELECT `data` FROM `". self::TABLE ."` WHERE `chid`=:chid AND `bot_id`=:bot")
                ->bind(":chid", $this->channel_id)
                ->bind(":bot", $_REQUEST['bot'])
                ->one();
            $data = json_decode($session['data'], true);
            return empty($data)? array() : $data;
        } else {
            return $this->global;
        }
    }

    public function store()
    {
        $data = json_encode($this->local + $this->data);

        $this->db
            ->query("UPDATE `". self::TABLE ."` SET `data`=:data WHERE `chid`=:chid AND `bot_id`=:bot")
            ->bind(':data', $data)
            ->bind(":bot", $_REQUEST['bot'])
            ->bind(':chid', $this->channel_id)
            ->execute();
    }

    public function add(...$args)
    {
        return $this->overload($args, [
            function($key, $value) {
                $this->local[$key] = $value;
            },
            function(array $local) {
                $this->local = $local + $this->local;
            }
        ]);
    }

    public function pop($offset)
    {
        $array = $this->offsetGet($offset);
        if (is_array($array)) {
            $value = array_shift($array);
            $this->offsetSet($offset, $array);
            return $value;
        }
        return null;
    }

    public function gd($offset)
    {
        $value = $this->offsetGet($offset);
        $this->offsetUnset($offset);
        return $value;
    }

    public function clear()
    {
        $this->data = [];
        $this->local = [];
        $this->store();
    }

    public function local(): array
    {
        return $this->local;
    }

    public function asArray(): array
    {
        return $this->data;
    }

    public function destroySession()
    {
        $this->db
            ->query("DELETE FROM `". self::TABLE ."` WHERE `chid`=:chid AND `bot_id`=:bot")
            ->bind(":chid", $this->channel_id)
            ->bind(":bot", $_REQUEST['bot'])
            ->execute();
    }

    public function offsetGet($offset)
    {
        $data = $this->local + $this->data;
        return $data[$offset] ?? null;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        $data = $this->local + $this->data;
        return isset($data[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
        unset($this->local[$offset]);
    }

    public function addTo($offset, $key, $value)
    {
        $data = $this->offsetGet($offset);
        if (is_null($data)) {
            $data = [];
        }

        if (null === $key) {
            $data[] = $value;
        } else {
            $data[$key] = $value;
        }
        $this->offsetSet($offset, $data);
    }

}