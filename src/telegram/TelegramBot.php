<?php

namespace telegram;

use bot\IBot;
use dto\Channel;
use dto\Message;
use dto\User;

use events\Event;

use events\Roles;
use logics\DB;
use logics\DbConnection;
use logics\Handler;
use services\Service;


class TelegramBot implements IBot
{
    use Event;
    /**
     * @var DbConnection
     */
    private $db;

    /**
     * @var TelegramApi
     */
    protected $api;

    /**
     * @var Channel
     */
    protected $channel;
    /**
     * @var User
     */
    protected $user;
    /**
     * @var Message
     */
    protected $message;
    /**
     * @var array
     */
    protected $callback;
    /**
     * @var array
     */
    protected $data;


    /**
     * @var SessionData
     */
    protected $session;

    protected $config;

    private $check_reply = true;

    public function __construct(string $token)
    {
        $this->api = new TelegramApi($token);
        $this->db = DbConnection::getInstance();

        $channel = $this->getChannel();

        $this->session = new SessionData($channel->Id());

        $this->config = Config::get();
    }

    private function underscoreToCamelCase($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace('_', '', ucwords($string, '_'));

        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }

        return $str;
    }

    public function handle()
    {
        $this->presession();

        $channel = $this->getChannel();
        $message = $this->getMessage();


        $this->session->add($message->Options());

        if ($message->isConverted()) {

            if ($this->channel->isListener()) {
                $message->setCommand(false);
                $message->setConverted(false);
                $message->setText($message->rawtext);
            } else {
                unset($this->session['select_wait']);
                $this->delete();
            }
        }

        if($message->isCommand()){

            if ($this->channel->isListener()) {
                if ($message->Text() !='start' ) {
                    return -1;
                }
            }

            $this->delegate($message->Text());

            $this->session();
        } else{
            $this->handlePlain();
        }
        $this->postsession();
    }

    private function presession()
    {
        $message = $this->getMessage();

        $this->session->add($message->Options());

        if (isset($this->session['delete_mid'])) {
            if ($message->isConverted()) {
                $this->deleteMessage($this->channel->Id(), $this->session->gd('delete_mid'));
            } else {
                unset($this->session['delete_mid']);
            }
        }
    }


    private function postsession()
    {
        unset($this->session['edit']);

        Config::get()->store();
        $this->session->store();
    }

    private function session()
    {
        if (isset($this->session['do_next'])) {
            $this->session['cmd'] = $this->session['do_next'];
            $this->delegate($this->session->gd('do_next'));
        }

        if (isset($this->session['next'])) {
            $this->session['do_next'] =  $this->session->gd('next');
        }

        if (isset($this->session['remove'])) {
            unset($this->session['remove']);
        }
        if(isset($this->session['temp'])) {
            $this->session['remove'] = $this->session->gd('temp');
        }
    }

    private function delegate(string $string, bool $edit = false)
    {
        $string = str_replace('/', '', $string);

        $routes = require DIR . '/src/events/routes.php';

        if (key_exists($string, $routes)) {
            $string = $routes[$string];
        }

        if (preg_match('/back_to_/', $string)) {
            $edit = true;

            if($string != 'back_to_panel') {
                $string = str_replace('back_to_', '', $string);
            }
        }

        $method = $this->underscoreToCamelCase($string) . 'Event';

        $data = $this->forTrait();
        $data[] = $edit;

        return call_user_func_array([$this, $method], $data);
    }

    private function deleteInputWaitMessage()
    {
        if (isset($this->session['input_wait'])) {
            // $this->deleteMessage($this->session['input_wait']['chid'], $this->session['input_wait']['mid']);
            unset($this->session['input_wait']);
        }
    }

    private function handlePlain()
    {
        if (isset($this->session['select_wait'])) {
            $this->delegate($this->session->gd('select_wait'));
        } elseif (isset($this->session['input_wait'])) {
            $to_delete = $this->delegate($this->session['input_wait']['cmd']);
            if ($to_delete !== false) {
                $this->deleteInputWaitMessage();
            }
        } else {
            if ($this->channel->isListener()) {

                $channel = Service::boundChannel($this->channel->Primary());

                $this->storeMessage();

                if ($this->checkQueue()) {

                    if (empty($channel['moder_chid'])) {
                        $channel['moder_chid'] = Service::defaultModerChid();
                        $channel['is_default'] = 1;
                        $channel['is_bind'] = false;
                    } else {
                        $channel['is_bind'] = true;
                    }

                    $this->sendToModerators($this->getUser(), $this->getMessage(), $channel);

                }
            }
        }
    }

    public function storeMessage()
    {
        $status = Handler::NON_MODERATED;
        $message = $this->getMessage();

        if (!empty($message->Caption())) {
            $text = $message->Text().'&#$'.$message->Caption();
        } else {
            $text = $message->Text();
        }

        if ($message->isEdited()) {
            $status = Handler::EDITED_AFTER_MSG;

            $old_message = Service::originMessage($message->Id());

            if (!empty($old_message) and in_array($old_message['status1'], [Handler::NON_MODERATED,Handler::EDITED_MSG])) {

                Service::updateMessage([
                    'status1' => Handler::EDITED_MSG,
                    'text'    => $text,
                    'format'  => $message->Type()
                ], $old_message['id']);

                $this->message->setPrimary($old_message['id']);
                return -1;

            } else {
                $last = Service::lastCondidat($message->Id());

                if (!empty($last)) {

                    Service::updateMessage([
                        'status1' => $status,
                        'text'    => $text,
                        'format'  => $message->Type()
                    ], $last['id']);

                    $this->message->setPrimary($last['id']);
                    return -1;
                }
            }
        }

        $moder = Service::boundChannel($this->channel->Primary());

        if (empty($moder['moder_id'])) {
            $moder_id = Service::defaultModerId();
        } else {
            $moder_id = $moder['moder_id'];
        }

        $message_id = Service::storeMessage([
            'bot_id'      => $_GET['bot'],
            'user_id'     => $this->user->Primary(),
            'channel_id'  => $this->channel->Primary(),
            'mid'         => $message->Id(),
            'text'        => $text,
            'format'      => $message->Type(),
            'status1'     => $status,
            'moder_id'    => $moder_id,
            'reply'       => $this->api->ReplyToMessageID()? $this->api->ReplyToMessageID() : null
        ]);

        $message->setPrimary($message_id);

    }

    public function forTrait()
    {
        $_user    = $this->getUser();
        $_message = $this->getMessage();
        $_channel = $this->getChannel();
        $_session = $this->session;
        return [$_user, $_message, $_channel, $_session];
    }


    public function openCloseButtons()
    {
        $keyboard = InlineKeyboardBuilder::create()
            ->row();

        if (isset($this->session['edit'])) {
            $keyboard
                ->addButton(Button::create('Открыть панель', '/oc_issue')->add($this->session->local()))
                ->addButton($this->closeButton());
        } else {
            $keyboard
                ->addButton(Button::create('Открыть панель', '/oc_panel')->add($this->session->local()))
                ->addButton($this->closeButton());
        }

        $this->editReply($keyboard->build());
    }

    public function sendDocument(string $document, $chat_id = null, array $params = [])
    {
        $chat_id    =  $chat_id? $chat_id : $this->getChannel()->Id();
        $this->checkForReply($params);
        return $this->api->sendDocument($params + compact('chat_id', 'document'));
    }

    public function sendLocation(string $location, $chat_id = null, array $params = [])
    {
        $parts = explode('&#$', $location);

        $latitude = $parts[0];
        $longitude = $parts[1];

        $chat_id    =  $chat_id? $chat_id : $this->getChannel()->Id();
        $this->checkForReply($params);
        return $this->api->sendLocation($params + compact('chat_id', 'latitude', 'longitude'));
    }

    public function sendPhoto(string $photo, $chat_id = null, array $params = [])
    {
        $chat_id    =  $chat_id? $chat_id : $this->getChannel()->Id();

        $this->checkForReply($params);
        return $this->api->sendPhoto($params + compact('chat_id', 'photo'));
    }

    public function sendVideo(string $video, $chat_id = null, array $params = [])
    {
        $chat_id    =  $chat_id? $chat_id : $this->getChannel()->Id();
        $this->checkForReply($params);
        return $this->api->sendVideo($params + compact('chat_id', 'video'));
    }

    public function sendVoice(string $voice, $chat_id = null, array $params = [])
    {
        $chat_id    =  $chat_id? $chat_id : $this->getChannel()->Id();
        $this->checkForReply($params);
        return $this->api->sendVoice($params + compact('chat_id',  'voice'));
    }

    public function sendAudio(string $audio, $chat_id = null, array $params = [])
    {
        $chat_id    =  $chat_id? $chat_id : $this->getChannel()->Id();
        $this->checkForReply($params);
        return $this->api->sendAudio($params + compact('chat_id', 'audio'));
    }

    public function sendAnimation(string $animation, $chat_id = null, array $params = [])
    {
        $chat_id    =  $chat_id? $chat_id : $this->getChannel()->Id();
        $this->checkForReply($params);

        return $this->api->sendAnimation($params + compact('chat_id', 'animation'));
    }

    public function sendSticker(string $sticker, $chat_id = null, array $params = [])
    {
        $chat_id = $chat_id? $chat_id : $this->getChannel()->Id();
        $this->checkForReply($params);
        return $this->api->sendSticker($params + compact('chat_id', 'sticker'));
    }

    public function sendMessage($chat_id, $text, array $params = [])
    {
        $this->checkForReply($params);
        return $this->api->sendMessage($params + compact('chat_id',  'text'));
    }

    public function deleteMessage($chat_id, $message_id)
    {
        return $this->api->deleteMessage(compact('chat_id', 'message_id'));
    }

    public function sendMessageMedia(string $type, $chat_id, $file_id, array $params = [])
    {
        switch ($type) {
            case Message::PHOTO:
                $result = $this->sendPhoto($file_id, $chat_id, $params);break;
            case Message::AUDIO:
                $result = $this->sendAudio($file_id, $chat_id, $params);break;
            case Message::VIDEO:
                $result = $this->sendVideo($file_id, $chat_id, $params);break;
            case Message::VOICE:
                $result = $this->sendVoice($file_id, $chat_id, $params);break;
            case Message::DOC:
                $result = $this->sendDocument($file_id, $chat_id, $params);break;
            case Message::ANIMATION:
                $result = $this->sendAnimation($file_id, $chat_id, $params);break;
            case Message::STICKER:
                $result = $this->sendSticker($file_id, $chat_id, $params);break;
            case Message::LOCATION:
                $result = $this->sendLocation($file_id, $chat_id, $params);break;
            default : $result = null;
        }
        return $result;
    }

    public function editMessageMedia(string $type, $chat_id, $message_id, $file_id, array $params = [])
    {
        $media = json_encode([
            'type'       => $type,
            'media'      => $file_id,
            'caption'    => $params['caption'] ?? '',
        ]);
        $this->checkForReply($params);
        return $this->api->editMessageMedia($params + compact('chat_id', 'media', 'message_id'));
    }

    public function delete()
    {
        $chat_id    = $this->getChannel()->Id();
        $message_id = $this->getMessage()->Id();
        return $this->api->deleteMessage(compact('chat_id', 'message_id'));
    }

    public function deleteReply($chat_id = null, $message_id = null)
    {
        $chat_id      =  $chat_id? $chat_id : $this->getChannel()->Id();
        $message_id   = $message_id? $message_id : $this->getMessage()->Id();
        $reply_markup = json_encode(['inline_keyboard' => []]);
        return $this->api->editMessageReplyMarkup(compact('chat_id', 'message_id', 'reply_markup'));
    }


    public function send($text, array $params = [])
    {
        return $this->sendMessage($this->getChannel()->Id(), $text, $params);
    }

    public function editReply($reply_markup, array $params = [])
    {
        return $this->editMessageReplyMarkup($this->getChannel()->Id(), $this->getMessage()->Id(), $reply_markup, $params);
    }

    public function editMessageReplyMarkup($chat_id, $message_id, $reply_markup, array $params = [])
    {
        return $this->api->editMessageReplyMarkup($params + compact('chat_id', 'message_id', 'reply_markup'));
    }

    public function edit($text, array $params = [])
    {
        return $this->editMessage($this->getChannel()->Id(), $this->getMessage()->Id(), $text, $params);
    }

    public function editMessage($chat_id, $message_id, $text, array $params = [])
    {
        $this->checkForReply($params);

        return $this->api->editMessageText($params + compact('chat_id', 'text', 'message_id'));
    }

    private function checkForReply(array & $params)
    {
        if ($this->check_reply == false) {
            $this->check_reply = true;
            return -1;
        }
        if ($this->channel->isModerator()) {
            $here = false;
            if (isset($this->session['cur_mid'])) {
                $message = Service::messageById($this->session->gd('cur_mid'));
            } elseif (isset($this->session['nnext'])) {
                $here = true;
                $message = Service::messageById($this->session->gd('nnext'));
            } else {
                $message = Service::messageByMid($this->message->Id());
            }

            if (empty($message['reply'])) {
                return -1;
            }

            $_message = Service::messageByMid($message['reply']);

            if ($here) {
                $params['reply_to_message_id'] = $_message['mid2'];
            } else {
                if ($_message['mid'] == $message['reply']) {
                    $params['reply_to_message_id'] = $_message['mid3'];
                } else {
                    $params['reply_to_message_id'] = $_message['mid'];
                }
            }

        } else {
            $message = Service::messageByMid($this->message->Id());

            if (empty($message['reply'])) {
                return -1;
            }

            $_message = Service::messageByMid($message['reply']);

            $params['reply_to_message_id'] = $_message['mid2'];
        }
    }

    private function nextMessage()
    {
        $this->check_reply = true;
        $message = Service::nearNotSentMessage($this->channel->Primary());
        if (! empty($message)) {
            $this->session['nnext'] = $message['id'];
            if ($message['format'] != 'text') {
                $text = explode('&#$', $message['text']);
                if ($message['format'] != 'location') {
                    $_message = Message::createMedia($message['mid'], strtotime($message['dat_add']), $text[0], $message['status1'] == 4);
                    $_message->setType($message['format']);

                    if (!empty($text[1])) {
                        $_message->setCaption($text[1]);
                    }

                } else {
                    $_message = Message::createMedia($message['mid'], strtotime($message['dat_add']), $message['text'], $message['status1'] == 4);
                    $_message->setType($message['format']);
                }
            } else {
                $_message = Message::create($message['mid'], strtotime($message['dat_add']), $message['text'], $message['status1'] == 4);
            }
            $_message->setPrimary($message['id']);


            $channel = Service::boundChannel($message['channel_id']);

            if (empty($channel['moder_chid'])) {
                $channel['is_bind'] = false;
            } else {
                $channel['is_bind'] = true;
            }
            $channel['moder_chid'] = $this->channel->Id();
            $this->sendToModerators($this->getUser(), $_message, $channel);
        }
    }

    private function checkQueue(): bool
    {
        $channel  = Service::boundChannel($this->channel->Primary());
        if (empty($channel['moder_id'])) {
            $admin = Service::defaultModerId();
        } else {
            $admin = $channel['moder_id'];
        }
        $not_sent = Service::notModeratedMessages($admin);
        return isset($not_sent[1])? false : true;
    }

    public function getUpdates(){
        $this->api->deleteWebhook();

        $updates = $this->api->getUpdates();

        $this->api->setWebhook(API_BOT_HOOK);

        return $updates;
    }

    public function isGroup(): bool
    {
        return $this->channel->isGroup();
    }

    public function isPrivate(): bool
    {
        return $this->channel->isPrivate();
    }

    public function isCommand(): bool
    {
        $message = $this->getMessage();
        return $message->isCommand();
    }

    public function isSingleText(): bool
    {
        return $this->isCommand() == false;
    }

    public function getData(): array
    {
        if(!$this->data){
            $this->data = $this->api->getData();
        }
        return $this->data;
    }

    public function getChannel(): Channel
    {
        if (!$this->channel) {
            $chat_id = $this->api->ChatID();
            $chat_info = $this->api->getChat(compact('chat_id'));

            $this->channel = Channel::create(
                $chat_info['result']['id'],
                $chat_info['result']['title'] ?? BOT_NAME,
                $chat_info['result']['type']
            );
        }
        return $this->channel;
    }


    private function filterText(string $text)
    {
        if (! empty($this->api->Entities())) {

            $users = DB::get()->run("SELECT * FROM `users`");

            $users_by_nam = [];
            $users_by_uid = [];

            $_offset = 0;
            $_total = 0;

            foreach ($users as $user) {
                $users_by_uid[$user['uid']] = $user['fak_nam'];

                if (! empty($user['nam'])) {
                    $users_by_nam[$user['nam']] = $user['fak_nam'];
                }
            }

            foreach ($this->api->Entities() as $entity) {
                $offset = $entity['offset'] - $_offset - $_total;
                $length = $entity['length'];

                $_total += $_offset;

                switch ($entity['type']) {
                    case 'mention':
                        $mention = mb_substr($text, $offset, $length);

                        if (isset($users_by_nam[$mention])) {
                            $text = mb_substr_replace($text, $users_by_nam[$mention], $offset, $length);
                            $_offset = $length - mb_strlen($users_by_nam[$mention]);
                        } else {
                            $text = mb_substr_replace($text, '#username', $offset, $length);
                            $_offset = $length - 9;
                        }

                        break;
                    case 'text_mention':

                        if (isset($users_by_uid[$entity['user']['id']])) {
                            $text = mb_substr_replace($text, $users_by_uid[$entity['user']['id']], $offset, $length);

                            $_offset = $length - mb_strlen($users_by_uid[$entity['user']['id']]);
                        } else {
                            $text = mb_substr_replace($text, '#username', $offset, $length);

                            $_offset = $length - 9;
                        }

                        break;
                    default:
                        continue 2;
                }
            }
        }

        return $text;
    }

    public function getMessage(): Message
    {
        if (!$this->message){

            if (empty($this->api->Text())) {

                switch ($this->api->getMessageType()) {
                    case Message::VOICE:
                        $file_id = $this->api->Voice()['file_id'];
                        break;
                    case Message::VIDEO:
                        $file_id = $this->api->Video()['file_id'];
                        break;
                    case Message::AUDIO:
                        $file_id = $this->api->Audio()['file_id'];
                        break;
                    case Message::PHOTO:
                        $file_id = $this->api->Photo()[0]['file_id'];
                        break;
                    case Message::ANIMATION:
                        $file_id = $this->api->Animation()['file_id'];
                        break;
                    case Message::DOC:
                        $file_id = $this->api->Document()['file_id'];
                        break;
                    case Message::STICKER:
                        $file_id = $this->api->Sticker()['file_id'];
                        break;
                    case Message::LOCATION:
                        $location = $this->api->Location();
                        $file_id = $location['latitude'].'&#$'.$location['longitude'];
                        break;
                    default:
                        die();
                }

                $this->message = Message::createMedia(
                    $this->api->MessageID(),
                    $this->api->Date(),
                    $file_id,
                    $this->api->getUpdateType() == $this->api::EDITED_MESSAGE
                );

                if (!empty($this->api->Caption())) {
                    $this->message->setCaption($this->filterText($this->api->Caption()));
                }

                $this->message->setType($this->api->getMessageType());
            } else {
                $this->message = Message::create(
                    $this->api->MessageID(),
                    $this->api->Date(),
                    $this->filterText($this->api->Text()),
                    $this->api->getUpdateType() == $this->api::EDITED_MESSAGE
                );
            }
        }
        return $this->message;
    }

    public function getUser(): User
    {
        if(!$this->user){
            $this->user = User::create(
                $this->api->UserID(),
                $this->api->Username(),
                $this->api->FirstName() . ' ' . $this->api->LastName()
            );
        }
        return $this->user;
    }


}







