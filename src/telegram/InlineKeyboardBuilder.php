<?php

namespace telegram;

class InlineKeyboardBuilder extends KeyboardBuilder
{

    public function build()
    {
        $this->end();

        extract(get_object_vars($this));

        $inline_keyboard = $this->keyboard;

        return json_encode(compact('inline_keyboard', 'resize_keyboard', 'one_time_keyboard', 'selective'));
    }
}