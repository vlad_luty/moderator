<?php

namespace telegram;


use logics\DbConnection;

class Config implements \ArrayAccess
{
    private $data;

    private static $instance;

    public static function get()
    {
        if (null == self::$instance) {
            self::$instance = new Config();
        }

        return self::$instance;
    }

    public function __construct()
    {
        /*$items = DbConnection::getInstance()
            ->query("SELECT `key`, `value`, `serialized` FROM `config`")
            ->all();

        if (!empty($items)) {
            foreach ($items as $item) {
                $key = $item['key'];
                if ($item['serialized']) {
                    $this->data[$key] = unserialize($item['value']);
                } else {
                    $this->data[$key] = $item['value'];
                }
            }
        }*/
    }

    public function store()
    {

    }

    public function offsetGet($offset)
    {
        return $this->data[$offset] ?? null;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }
}