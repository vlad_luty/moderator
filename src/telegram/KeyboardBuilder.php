<?php

namespace telegram;
use traits\Overloadable;


abstract class KeyboardBuilder
{
    use Overloadable;

    /**
     * @var array
     */
    protected $row;

    /**
     * @var Button[]
     */
    protected $keyboard = [];

    /**
     * @var bool
     */
    protected $one_time_keyboard = false;

    /**
     * @var bool
     */
    protected $resize_keyboard = false;

    /**
     * @var bool
     */
    protected $selective = false;

    protected function __construct(){ }

    public static function create(): self
    {
        $builder = new static();
        return $builder;
    }

    public function row(): self
    {
        $this->row = [];
        return $this;
    }

    public function end(): self
    {
        if(!empty($this->row)) {
            $this->keyboard[] = $this->row;
        }

        $this->row = [];
        return $this;
    }

    public function add(...$args): self
    {
        return $this->overload($args, [
            function (Button ...$buttons) {
                return $this->addButtons(...$buttons);
            },
            function (array $data) {
                return $this->addArray($data, 1);
            },
            function (array $data, int $col) {
                return $this->addArray($data, $col);
            },
            function(array $data, array $options) {
                return $this->addWithData($data, 1, $options);
            },
            function(array $data, int $col, array $options) {
                return $this->addWithData($data, $col, $options);
            }
        ]);
    }

    public function addWithData(array $data, int $col, array $options): self
    {
        $this->row();
        $count = 0;

        foreach ($data as $item) {

            if(($count++ % $col) == 0) {
                $this->end();
            }

            if(is_array($item) && !isset($item['text'])) {
                foreach ($item as $subitem) {
                    $this->addButton(Button::create($subitem['text'], $subitem['cmd'], $options));
                }
            } else {
                $this->addButton(Button::create($item['text'], $item['cmd'], $options));
            }

        }
        return $this;
    }

    public function addArray(array $data, int $col = 1): self
    {
        $this->row();
        $count = 0;

        foreach ($data as $item) {

            if(($count++ % $col) == 0) {
                $this->end();
            }

            if(is_array($item) && !isset($item['text'])) {
                foreach ($item as $subitem) {
                    $this->addButton(Button::create($subitem['text'], $subitem['cmd'], $subitem['data'] ?? []));
                }
            } else {
                $this->addButton(Button::create($item['text'], $item['cmd'], $item['data'] ?? []));
            }
        }
        $this->end();
        return $this;
    }


    public function addButton(Button $button): self
    {
        $this->row[] = $button->inline();
        return $this;
    }

    public function addButtons(Button ...$buttons): self
    {
        foreach ($buttons as $button){
            $this
                ->row()
                ->addButton($button)
                ->end();
        }
        return $this;
    }

    public function resize_able(): self
    {
        $this->resize_keyboard = true;
        return $this;
    }

    public function one_time(): self
    {
        $this->one_time_keyboard = true;
        return $this;
    }

    public function selective(): self
    {
        $this->selective = true;
        return $this;
    }

    public static function fullCreate(array $buttons)
    {
        $keyboard = new static();
        foreach ($buttons as $button) {
            $keyboard->row();
            if (is_array($button)) {
                foreach ($button as $btn) {
                    $keyboard->addButton($btn);
                }
            } else {
                $keyboard->addButton($button);
            }
            $keyboard->end();
        }
        return $keyboard->build();
    }

    abstract public function build();
}