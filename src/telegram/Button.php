<?php

namespace telegram;

use traits\Overloadable;

class Button
{
    use Overloadable;

    const CALLBACK_BUTTON = 'callback_query';

    const URL_BUTTON = 'url';

    /**
     * @var string
     */
    protected $text;

    /**
     * @var string
     */
    protected $command;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var string
     */
    protected $type;


    protected function __construct(string $text, string $command)
    {
        $this->text = $text;
        $this->command = $command;
        $this->data = [];
        $this->type = self::CALLBACK_BUTTON;
    }

    public static function create(string $text, string $command, array $data = []): self
    {
        $static = new self($text, $command);
        $static->add($data);
        return $static;
    }

    public static function fromArray(array $params): self
    {
        return self::create($params['text'], $params['cmd'], $params['data'] ?? []);
    }

    public function asUrl(): self
    {
        $this->type = self::URL_BUTTON;
        return $this;
    }

    public function asButton(): self
    {
        $this->type = self::CALLBACK_BUTTON;
        return $this;
    }

    public function add(...$args): self
    {
        return $this->overload($args, [
            function ($key, $value) {
                $this->data[$key] = $value;
                return $this;
            },
            function (array $array) {
                foreach ($array as $k => $v) {
                    $this->data[$k] = $v;
                }
                return $this;
            },
        ]);
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    public function getCommand(): string
    {
        return $this->command;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isUrl(): bool
    {
        return $this->getType() == self::URL_BUTTON;
    }

    public function isButton(): bool
    {
        return $this->getType() == self::CALLBACK_BUTTON;
    }

    public function inline(): array
    {
        $this->data['cmd'] = $this->getCommand();

        $inline_button = [
            'text' => $this->getText(),
        ];

        switch ($this->getType()){
            case self::URL_BUTTON:
                $inline_button['url'] = $this->getCommand();
                break;
            default:
                $inline_button['callback_data'] = json_encode($this->data);
                break;
        }
        return $inline_button;
    }

}