<?php
namespace telegram;


class ReplyKeyboardBuilder extends KeyboardBuilder
{

    public function build()
    {
        $this->end();

        extract(get_object_vars($this));

        $keyboard = $this->keyboard;

        return json_encode(compact('keyboard', 'resize_keyboard', 'one_time_keyboard', 'selective'));
    }
}