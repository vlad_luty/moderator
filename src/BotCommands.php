<?php


interface BotCommands
{
    const CLOSE_CMD = '/close';
    const HIDE_CMD  = '/hide';

    const PANEL_CMD = '/panel';
}