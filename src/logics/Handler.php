<?php

namespace logics;

use dto\Channel;
use dto\Message;
use dto\User;
use Faker\Factory;
use services\ChannelService;
use services\JiraService;
use services\MessageService;
use services\Service;
use services\SessionService;
use services\UserService;
use bot\IBot;
use telegram\Button;
use telegram\InlineKeyboardBuilder;
use telegram\SessionData;
use telegram\TelegramBot;
use utils\Log;

class Handler
{

    const NON_MODERATED = 0;
    const OK = 1;
    const BLOCKED = 2;
    const EDITED = 3;
    const EDITED_MSG = 4;
    const IN_EDIT = 5;
    const EDITED_AFTER_MSG = 6;

    const NOT_BIND_MSG = 7;

    /**
     * @var Channel
     */
    private $channel;

    /**
     * @var User
     */
    private $user;
    /**
     * @var Message
     */
    private $message;

    /**
     * @var DbConnection
     */
    private $db;

    /**
     * @var TelegramBot
     */
    private $bot;

    public function __construct(TelegramBot $bot)
    {
        $this->bot = $bot;
        $this->db = DbConnection::getInstance();
    }

    public function webhook(){
        $this->channel = $this->bot->getChannel();
        $this->user = $this->bot->getUser();
        $this->message = $this->bot->getMessage();

        $this->storeUser();
        $this->storeChat();


        $this->bot->handle();
    }

    public function getMessage()
    {
        return $this->message;
    }


    private function storeChat()
    {
        $channel = DB::get()->row("SELECT `id`, `chid`, `mode` FROM `channels` ".
            "WHERE `chid`=? AND `bot_id`=?",
            $this->channel->Id(), BOT_ID
        );

        if (empty($channel)) {
            DB::get()->insert('channels', [
                'bot_id' => BOT_ID,
                'chid'   => $this->channel->Id(),
                'type'   => $this->channel->isGroup()? 'group' : 'private',
                'name'   => $this->channel->isGroup()? $this->channel->Name() : $this->user->Fio(),
                'mode'   => (BOT_ID == 2)? Channel::TESTER_MODE : $this->channel->Mode()
            ]);

            $channel_mode = (BOT_ID == 2)? Channel::TESTER_MODE : $this->channel->Mode();
            $channel_id   = DB::get()->lastInsertId();
        } else {
            $channel_id = $channel['id'];
            $channel_mode = $channel['mode'];
        }


        $binding_id = DB::get()->cell("SELECT `id` FROM `channel_to_channel` WHERE (`client_id`=? or `moder_id`=? or `developer_id`=?) AND `bot_id`=?",
            $channel_id,$channel_id,$channel_id, BOT_ID);

        if (empty($binding_id)) {
            $default = Service::defaultModerId();
            if (!empty($default)) {
                DB::get()->insert('channel_to_channel', [
                    'bot_id' => BOT_ID,
                    'client_id' => $channel_id,
                    'developer_id' => 0,
                    'moder_id' =>$default
                ]);
            }

        }

        $this->channel->setName($this->channel->isGroup()? $this->channel->Name() : BOT_NAME);
        $this->channel->setMode($channel_mode);
        $this->channel->setPrimary($channel_id);
    }

    private function storeUser()
    {
        $user = DB::get()->row("SELECT `id`, `nam`, `fio`, `fak_nam` FROM `users` WHERE `uid`=?", $this->user->Id());

        $user_id = $user['id'];

        $faker = Factory::create('Ru_RU');

        if (empty($user_id)) {
            $name = explode(' ', $faker->name);

            DB::get()->insert('users', [
                'uid' => $this->user->Id(),
                'fio' => $this->user->Fio(),
                'nam' => $this->user->Username(),
                'fak_nam' => $name[0].' '.$name[1],
            ]);
            $user_id = DB::get()->lastInsertId();
            $name = $name[0].' '.$name[1];
        } else {

            $data = [];

            if (empty($user['fak_nam'])) {
                $name = explode(' ', $faker->name);
                $data['fak_nam'] = $name[0].' '.$name[1];
            }

            if ($user['fio'] != $this->user->Fio()) {
                $data['fio'] = $this->user->Fio();
            }

            if ($user['nam'] != $this->user->Username()) {
                $data['nam'] = $this->user->Username();
            }

            if (!empty($data)) {
                DB::get()->update('users', $data , ['id' => $user_id]);
            }

            $name = DB::get()->cell("SELECT `fak_nam` FROM `users` WHERE `id`=?", $user_id);
        }

        $this->user->setNam($name);
        $this->user->setPrimary($user_id);
    }

}