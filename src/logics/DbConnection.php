<?php

namespace logics;

class DbConnection
{
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;

    /**
     * @var \PDO
     */
    private $dbh;

    /**
     * @var \PDOStatement
     */
    private $stmt;

    /**
     * @var DbConnection
     */
    private static $instance;

    public static function getInstance(): DbConnection
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __construct()
    {
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname . ';charset=utf8';
        $options = array(
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
        );

        $this->dbh = new \PDO($dsn, $this->user, $this->pass, $options);
    }

    public function __invoke(string $statement)
    {
        return $this->dbh->query($statement);
    }


    public function rawQuery($sql) : \PDOStatement
    {
        return $this->dbh->query($sql);
    }
    // Prepare statement with query
    public function query($sql) : self
    {
        $this->stmt = $this->dbh->prepare($sql);
        return $this;
    }

    // Bind values
    public function bind($param, $value, $type = null):self
    {
        if(is_null($type)){
            switch(true){
                case is_int($value):
                    $type = \PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = \PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = \PDO::PARAM_NULL;
                    break;
                default:
                    $type = \PDO::PARAM_STR;
            }
        }

        $this->stmt->bindValue($param, $value, $type);
        return $this;
    }

    // Execute the prepared statement
    public function execute()
    {
        return $this->stmt->execute();
    }

    // Get result set as array of objects
    public function all(int $fetch_style = \PDO::FETCH_ASSOC)
    {
        $this->execute();

        return $this->stmt->fetchAll($fetch_style);
    }

    // Get single record as object
    public function one(int $fetch_style = \PDO::FETCH_ASSOC)
    {
        $this->execute();

        return $this->stmt->fetch($fetch_style);
    }

    public function column()
    {
       if ($this->execute()) {
           return $this->stmt->fetchColumn();
       } else {
           return null;
       }
    }

    // Get row count
    public function rowCount() : int
    {
        return $this->stmt->rowCount();
    }

    public function lastInsertId()
    {
        return $this->dbh->lastInsertId();
    }
}