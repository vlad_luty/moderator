<?php
namespace logics;

use ParagonIE\EasyDB\EasyDB;


final class DB
{
    /**
     * @var EasyDB
     */
    private $db;

    /**
     * @var DB
     */
    private static $instance = null;

    public static function get(): EasyDB
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance->db;
    }

    private function __construct()
    {
        $this->db = \ParagonIE\EasyDB\Factory::create(
            'mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4',
            DB_USER,
            DB_PASS
        );
    }
}