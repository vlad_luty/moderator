<?php
namespace events;

use telegram\Button;


trait ButtonsTrait
{
    public function urlButton($name, $url)
    {
        return Button::create($name, $url)->asUrl();
    }

    public function commandButton(string $name, string $cmd, array $options = [])
    {
        return Button::create($name, $cmd, $options);
    }

    public function closeButton(array $options = [])
    {
        return Button::create('Закрыть', \BotCommands::CLOSE_CMD, $options);
    }
    
    public function hideButton(array  $options = [])
    {
        return Button::create('Скрыть', \BotCommands::HIDE_CMD, $options);
    }

    public function backToStatusesButton(array $options = [])
    {
        return Button::create('Вернуться', '/back_to_statuses', $options);
    }

    public function backToTeamIssuesButton(array $options = [])
    {
        return Button::create('Вернуться', '/back_to_team_issues', $options);
    }

    public function backToSelectIssuesButton(array $options = [])
    {
        return Button::create('Вернуться', '/back_to_select_issues', $options);
    }

    public function backToPanelButton(array $options = [])
    {
        return Button::create('Вернуться', '/back_to_panel', $options);
    }

    public function backToLabelsButton(array $options = [])
    {
        Button::create('Вернуться', '/back_to_labels', $options);
    }

    public function backToWorkersButton(array $options = [])
    {
        return Button::create('Вернуться', '/back_to_workers', $options);
    }

    public function backToSprintsButton(array $options = [])
    {
        return Button::create('Вернуться', '/back_to_sprints', $options);
    }

    public function issueButton(array $options = [])
    {
        return Button::create('Больше', '/issue', $options);
    }

    public function panelButton(array $options = [])
    {
        return Button::create('Больше', \BotCommands::PANEL_CMD, $options);
    }

    public function selectSpeakerButton(array $options = [])
    {
        return Button::create('Пообщаться', '/select_speaker', $options);
    }

    public function assigneeButton(array $options = [])
    {
        return Button::create('Назначить', '/assignee', $options);
    }

    public function printCommentButton(array $options = [])
    {
        return Button::create('Написать комментарий', '/print_comment', $options);
    }

    public function takeIssueButton(array $options = [])
    {
        return Button::create('Взять в работу', '/take_issue', $options);
    }

    public function doneIssueButton(array $options = [])
    {
        return Button::create('Закончить работу', '/done_issue', $options);
    }

    public function delayIssueButton(array $options = [])
    {
        return Button::create('Отложить работу', '/delay_issue', $options);
    }

    public function statusesButton(array $options = [])
    {
        return Button::create('Статусы', '/statuses', $options);
    }

}