<?php
namespace events;

use dto\Channel;
use dto\Message;
use dto\User;
use logics\DB;
use logics\DbConnection;
use logics\Handler;
use services\Service;
use telegram\Button;
use telegram\Config;
use telegram\InlineKeyboardBuilder;
use telegram\ReplyKeyboardBuilder;
use telegram\SessionData;


trait TestModerateEvent
{

    public function okTestEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $message = Service::testMessageById($_session->gd('mid'));

        if (Handler::EDITED_AFTER_MSG == $message['status1']) {
            Service::deleteTestById($message['id']);

            $old_message = Service::originMessage($message['mid']);

            Service::updateMessageForTest([
                'status1' => Handler::OK,
                'text'    => $message['text']
            ], $old_message['id']);

        } else {
           Service::updateMessageForTest([
                'status1' => Handler::OK
            ], $message['id']);
        }

        $this->deleteReply();

        $this->nextTestMessage($_channel->Primary());
    }

    public function blockTestEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $message = Service::testMessageById($_session->gd('mid'));

        if (Handler::EDITED_AFTER_MSG == $message['status1']) {
            Service::deleteTestById($message['id']);
        } else {
            Service::updateMessageForTest([
                'status1' => Handler::BLOCKED
            ], $message['id']);
        }

        $this->deleteReply();

        if (Handler::EDITED_AFTER_MSG == $message['status1']) {
            $this->delete();
        }
        $this->nextTestMessage($message['channel_id']);
    }

    public function edittTestEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $message = Service::testMessageById($_session['mid']);

        $_session['cur_mid'] = $_session->gd('mid');

        $_session['in_edit'] = $_message->Id();

        $this->editMessageReplyMarkup($_channel->Id(),  $_message->Id(), InlineKeyboardBuilder::create()->addButton(Button::create('Изменяется',''))->build());

        $reply = ReplyKeyboardBuilder::create()->resize_able()
            ->addButton(Button::create('Отмена', '/cancel'))
            ->build();

        $this->check_reply = false;

        $text = explode("&#$", $message['text']);

        $result = $this->sendMessageMedia($message['format'], null, $text[0], [
            'reply_markup' => $reply,
            'caption'      => $text[1] ?? ''
        ]);

        $_session['artifact'] = $result['result']['message_id'];

        $_session['last'] = 'mod_message';

        $_session['input_wait'] = ['cmd' => '/editt_message_test'];
    }

    public function editTestEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $message = Service::testMessageById($_session['mid']);

        $_session['cur_mid'] = $_session->gd('mid');

        $_session['in_edit'] = $_message->Id();

        $this->editMessageReplyMarkup($_channel->Id(),  $_message->Id(), InlineKeyboardBuilder::create()->addButton(Button::create('Изменяется',''))->build());

        $reply = ReplyKeyboardBuilder::create()->resize_able()
            ->addButton(Button::create('Отмена', '/cancel'))
            ->build();

        $this->check_reply = false;

        switch ($message['format']) {
            case Message::TEXT:
                $result = $this->send("Изменить соообщение {$message['text']} {$message['from']}", [
                    'reply_markup' => $reply
                ]);
                break;
            default:
                $text = explode("&#$", $message['text']);

                $result = $this->sendMessageMedia($message['format'], null, $text[0], [
                    'reply_markup' => $reply,
                    'caption'      => $text[1] ?? ''
                ]);
                break;
        }

        $_session['artifact'] = $result['result']['message_id'];

        $_session['last'] = 'mod_message';

        $_session['input_wait'] = ['cmd' => '/edit_message_test'];
    }

    public function editMessageTestEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $_session->gd('input_wait');

        if(!empty($_message->Caption())) {
            $text = $_message->Text().'&#$'.$_message->Caption();
        } else {
            $text = $_message->Text();
        }

        $message = Service::testMessageById($_session['cur_mid']);

        if ($message['status1'] == 4) {
            Service::deleteTestById($message['id']);

            $old_message = Service::originMessage($message['mid']);

            Service::updateMessageForTest([
                'text'    => $text,
                'format'  => $_message->Type(),
                'status1' => Handler::EDITED
            ], $old_message['id']);

        } else {
            Service::updateMessageForTest([
                'text'    => $text,
                'format'  => $_message->Type(),
                'status1' => Handler::EDITED
            ], $message['id']);
        }

        $this->deleteReply(null, $_session['in_edit']);
        $result = $this->send('_', ['reply_markup' => $this->getEmptyMenu()]);
        $this->delete();
        $this->deleteMessage($_channel->Id(), $result['result']['message_id']);
        $this->deleteArtifact();

        $this->nextTestMessage($_channel->Primary());
    }

    public function edittMessageTestEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $_session->gd('input_wait');

        $text = $_message->Text();

        $message = Service::testMessageById($_session['cur_mid']);

        $old = explode('&#$', $message['text']);

        if ($message['status1'] == 4) {
            Service::deleteTestById($message['id']);

            $old_message = Service::originMessage($message['mid']);

            Service::updateMessageForTest([
                'text'    => $old.'&#$'.$text,
                'status1' => Handler::EDITED
            ], $old_message['id']);

        } else {
            Service::updateMessageForTest([
                'text'    => $old[0].'&#$'.$text,
                'status1' => Handler::EDITED
            ], $message['id']);
        }

        $this->deleteReply(null, $_session['in_edit']);
        $result = $this->send('_', ['reply_markup' => $this->getEmptyMenu()]);
        $this->delete();
        $this->deleteMessage($_channel->Id(), $result['result']['message_id']);
        $this->deleteArtifact();

        $this->nextTestMessage($_channel->Primary());
    }
}