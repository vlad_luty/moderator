<?php
namespace events;


use dto\Channel;
use dto\Message;
use dto\User;
use logics\Handler;
use services\Service;
use telegram\SessionData;

trait CancelEvent
{
    public function cancelEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $this->deleteArtifact();
        unset($_session['input_wait'], $_session['select_wait']);
        $this->delegate($_session->gd('last'));
    }

    public function closeEvent()
    {
        $this->delete();
    }

}