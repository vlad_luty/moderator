<?php
namespace events;

use dto\Channel;
use dto\Message;
use dto\User;
use logics\DB;
use logics\DbConnection;
use logics\Handler;
use services\Service;
use telegram\Button;
use telegram\Config;
use telegram\InlineKeyboardBuilder;
use telegram\ReplyKeyboardBuilder;
use telegram\SessionData;


trait TesterEvent
{
    public function sendDialogEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $dialogs = Service::dialogs();

        if(empty($dialogs)) {
            return $this->send('Список диалогов пуст');
        }

        foreach ($dialogs as $dialog) {
            $item[] = [
                'text' =>
                    $dialog['dialog_id'].'|'
                    .date('d.m H:i', strtotime($dialog['dat_add']))
                    .'|'.substr($dialog['text'], 0, 40).'...',
                'cmd'  => ''
            ];
        }

        $keyboard = ReplyKeyboardBuilder::create()->resize_able()
            ->add($item, 2)
            ->addButton(Button::create('На главную', ''));
        $this->switchToSelectionMode('dialog');
        $this->send('Выберите диалог', ['reply_markup'=>$keyboard->build()]);
    }

    public function dialogEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $parts = explode('|', $_message->rawtext);

        $_session['dialog_id'] = $parts[0];

        $moderators = Service::testers();

        $keyboard = ReplyKeyboardBuilder::create();

        foreach ($moderators as $moderator) {
            $item[] = [
                'text' => $moderator['name'],
                'cmd'  => ''
            ];
        }
        $keyboard
            ->resize_able()
            ->add($item, 2)
            ->addButton(Button::create('На главную', ''));

        $this->switchToSelectionMode('selected_for_send');
        $this->send('Выберите кому отправить диалог', ['reply_markup'=>$keyboard->build()]);
    }

    public function selectedForSendEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $channel = Service::channelByName($_message->rawtext);

        $messages = Service::dialogById(intval($_session->gd('dialog_id')));

        foreach ($messages as $message) {

            $message_id = \logics\DB::get()->cell("SELECT `id` FROM `test` WHERE `channel_id` = ? AND origin_id = ?", $channel['id'], $message['id']);
            if (empty($message_id)) {
                Service::storeMessageForTest([
                    'channel_id' => $channel['id'],
                    'text'       => $message['text'],
                    'format'     => $message['format'],
                    'status1'    => 0,
                    'origin_id'  => $message['id'],
                ]);
            }
        }
        $this->nextTestMessage($channel['id']);
        $this->delegate('panel');
    }

    public function nextTestMessage($channel_id)
    {

        $message = Service::testMessageNotModerated($channel_id);

        if (!empty($message)) {

            $name = $message['name'].' : ';

            $keyboard = \telegram\InlineKeyboardBuilder::create()
                ->addButton(Button::create("\u{2705} OK", 'ok_test', ['mid' => $message['id']]))
                ->addButton(Button::create("\u{26D4} БАН", 'block_test', ['mid' => $message['id']]))
                ->addButton(Button::create("\u{270F} ИЗМЕНИТЬ", 'edit_test', ['mid' => $message['id']]));


            if (in_array($message['format'] , [Message::TEXT, Message::STICKER, Message::LOCATION]) === false) {
                $keyboard->addButton(Button::create("\u{270F} ПОДПИСЬ", 'editt_test', ['mid' => $message['id']]));
            }

            $reply_markup = $keyboard->build();

            $text = explode("&#$", $message['text']);

            switch ($message['format']) {
                case Message::TEXT:
                    $this->sendMessage(
                        $message['chid'],
                        $name.$message['text'],
                        compact('reply_markup')
                    );
                    break;
                default:
                    $this->sendMessageMedia(
                        $message['format'],
                        $message['chid'],
                        $text[0],[
                            'caption' => $name.$text[1],
                            'reply_markup' => $reply_markup
                        ]
                    );
            }
        }
    }


}