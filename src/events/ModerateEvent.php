<?php
namespace events;

use dto\Channel;
use dto\Message;
use dto\User;
use logics\DB;
use logics\DbConnection;
use logics\Handler;
use services\Service;
use telegram\Button;
use telegram\Config;
use telegram\InlineKeyboardBuilder;
use telegram\ReplyKeyboardBuilder;
use telegram\SessionData;


trait ModerateEvent
{

    public function okEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {

        $message = Service::messageById($_session->gd('mid'));

        $channel = Service::boundChannel($message['channel_id']);

        if (Handler::EDITED_AFTER_MSG == $message['status1']) {
            Service::deleteMessageById($message['id']);

            $old_message = Service::originMessage($message['mid']);

            $message = Service::updateMessage([
                'status1' => Handler::OK,
                'text'    => $message['text']
            ], $old_message['id']);

        } else {
            $message = Service::updateMessage([
                'status1' => Handler::OK
            ], $message['id']);
        }


        $text = explode("&#$", $message['text']);

        if ('client' === $channel['role']) {
            if (! empty($text[1])) {
                $caption = $message['from'].' : ' . $text[1];
                $text = $text[0];
            } else {
                if ('text' == $message['format']) {
                    $text = $message['from'].' : ' . $text[0];
                } else {
                    $text = $text[0];
                }
                $caption = $message['from'];
            }
        } else {
            $caption = $text[1] ?? '';
            $text = $text[0];
        }
        
        if ($message['format'] == 'location') {
            $text = $message['text'];
        }

        if (isset($old_message) and (is_null($message['mid3']) == false)) {

            $this->editMessageWithContent($channel['chid'],  $message['mid3'], $text, $message['format'], $caption);
        } else {
            $result = $this->sendMessageWithContent($channel['chid'], $text, $message['format'], $caption);
            Service::updateMessage(['mid3' => $result['result']['message_id']], $message['id']);
        }


        $this->deleteReply();

        if (isset($old_message)) {
            $this->deleteMessage($_channel->Id(), $_message->Id());
            $mid = $old_message['mid2'];
            $text = "\u{2705}".date('H:i', strtotime($message['dat_add'])).' '. $message['from']."\n";
            if (Service::defaultModerId() == $_channel->Primary()) {
                $text .= $_channel->isPrivate()
                    ? "изменил в личке\n"
                    : "изменил в группе ".Service::channelName($old_message['channel_id'])."\n";
            }
        } else {
            $mid = $_message->Id();
            $text = "\u{2705}".date('H:i', strtotime($message['dat_add'])).' '. $message['from']."\n";
            if (Service::defaultModerId() == $_channel->Primary()) {
                $text .= $_channel->isPrivate()
                    ? "написал в личке\n"
                    : "написал в группе ".Service::channelName($message['channel_id'])."\n";
            }
        }

        switch ($message['format']) {
            case Message::TEXT:
                $this->editMessage($_channel->Id(), $mid, $text.$message['text']);
                break;
            case Message::LOCATION:
            case Message::STICKER:
                $this->editMessageReplyMarkup($_channel->Id(), $mid, $this->getOkReply());
                break;
            default:
                $parts = explode('&#$', $message['text']);
                $this->editMessageMedia($message['format'], $_channel->Id(), $mid, $parts[0], ['caption' => $text.($parts[1]??'')]);
        }

        $this->nextMessage();
    }

    public function blockEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {

        $message = Service::messageById($_session->gd('mid'));

        if (Handler::EDITED_AFTER_MSG == $message['status1']) {
            Service::deleteMessageById($message['id']);
        } else {
            Service::updateMessage([
                'status1' => Handler::BLOCKED
            ], $message['id']);
        }

        $this->deleteReply();

        if (Handler::EDITED_AFTER_MSG == $message['status1']) {
            $this->delete();
            $this->nextMessage();
            return -1;
        }

        $text = "\u{26D4}".date('H:i', strtotime($message['dat_add'])).' '. $message['from']."\n";

        if (Service::defaultModerId() == $_channel->Primary()) {
            $text .= $_channel->isPrivate()
                ? "написал в личке\n"
                : "написал в группе ".Service::channelName($message['channel_id'])."\n";
        }
        switch ($message['format']) {
            case Message::TEXT:
                $this->editMessage($_channel->Id(), $_message->Id(), $text.$message['text']);
                break;
            case Message::LOCATION:
            case Message::STICKER:
                $this->editMessageReplyMarkup($_channel->Id(), $_message->Id(),$this->getBlockReply());
                break;
            default:
                $parts = explode('&#$', $message['text']);
                $this->editMessageMedia($message['format'], $_channel->Id(), $_message->Id(), $parts[0], ['caption' => $text.($parts[1]??'')]);
        }

        $this->nextMessage();
    }


    public function editEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $message = Service::messageById($_session['mid']);

        $_session['cur_mid'] = $_session->gd('mid');

        $_session['in_edit'] = $_message->Id();

        $this->editMessageReplyMarkup($_channel->Id(),  $_message->Id(), InlineKeyboardBuilder::create()->addButton(Button::create('Изменяется',''))->build());

        $reply = ReplyKeyboardBuilder::create()->resize_able()
            ->addButton(Button::create('Отмена', '/cancel'))
            ->build();

        $this->check_reply = false;

        switch ($message['format']) {
            case Message::TEXT:
                $result = $this->send("Изменить соообщение {$message['text']} {$message['from']}", [
                    'reply_markup' => $reply
                ]);
                break;
            default:
                $text = explode("&#$", $message['text']);

                $result = $this->sendMessageMedia($message['format'], null, $text[0], [
                    'reply_markup' => $reply,
                    'caption'      => $text[1] ?? ''
                ]);
                break;
        }

        $_session['artifact'] = $result['result']['message_id'];

        $_session['last'] = 'mod_message';

        $_session['input_wait'] = ['cmd' => '/em'];
    }

    public function replyEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $message = Service::messageById($_session['mid']);

        $_session['cur_mid'] = $_session->gd('mid');

        $_session['in_edit'] = $_message->Id();

        $this->editMessageReplyMarkup($_channel->Id(),  $_message->Id(), InlineKeyboardBuilder::create()->addButton(Button::create('Готовится ответ',''))->build());

        $reply = ReplyKeyboardBuilder::create()->resize_able()
            ->addButton(Button::create('Отмена', '/cancel'))
            ->build();

        $this->check_reply = false;

        switch ($message['format']) {
            case Message::TEXT:
                $result = $this->send("Ответить на сообщение {$message['text']} {$message['from']}", [
                    'reply_markup' => $reply
                ]);
                break;
            default:
                $text = explode("&#$", $message['text']);

                $result = $this->sendMessageMedia($message['format'], null, $text[0], [
                    'reply_markup' => $reply,
                    'caption'      => $text[1] ?? ''
                ]);
                break;
        }

        $_session['artifact'] = $result['result']['message_id'];

        $_session['last'] = 'dom_message';

        $_session['input_wait'] = ['cmd' => '/rm'];
    }

    public function replyMessageEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $_session->gd('input_wait');

        $before_edit_message = Service::messageById($_session->gd('cur_mid'));
        if (Handler::EDITED_AFTER_MSG == $before_edit_message['status1']) {
            Service::deleteMessageById($before_edit_message['id']);

            $old_message = Service::originMessage($before_edit_message['mid']);

            $message = Service::updateMessage([
                'status1' => Handler::EDITED
            ], $old_message['id']);

        } else {
            $message = Service::updateMessage([
                'status1' => Handler::EDITED
            ], $before_edit_message['id']);
        }

        $receiver = Service::channelById($message['channel_id']);

        $this->sendMessageWithContent($receiver['chid'], $_message->Text(), $_message->Type(), $_message->Caption());

        $this->deleteReply(null, $_session['in_edit']);

        $result = $this->send('_', ['reply_markup' => $this->getEmptyMenu()]);
        $this->delete();
        $this->deleteMessage($_channel->Id(), $result['result']['message_id']);
        $this->deleteArtifact();

        $this->nextMessage();

    }

    public function editMessageEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $_session->gd('input_wait');

        if(!empty($_message->Caption())) {
            $text = $_message->Text().'&#$'.$_message->Caption();
        } else {
            $text = $_message->Text();
        }

        $before_edit_message = Service::messageById($_session['cur_mid']);

        $channel = Service::boundChannel($before_edit_message['channel_id']);


        if (Handler::EDITED_AFTER_MSG == $before_edit_message['status1']) {
            Service::deleteMessageById($before_edit_message['id']);

            $old_message = Service::originMessage($before_edit_message['mid']);

            $message = Service::updateMessage([
                'text'    => $text,
                'format'  => $_message->isVoice()? 'text' : $_message->Type(),
                'status1' => Handler::EDITED
            ], $old_message['id']);

        } else {
            $message = Service::updateMessage([
                'text'    => $text,
                'format'  => $_message->isVoice()? 'text' : $_message->Type(),
                'status1' => Handler::EDITED
            ], $before_edit_message['id']);
        }


        $text = explode("&#$", $message['text']);
        if ('client' === $channel['role']) {
            if (! empty($text[1])) {
                $caption = $message['from'].': ' . $text[1];
                $text = $text[0];
            } else {
                if ('text' == $message['format']) {
                    $text = $message['from'].': ' . $text[0];
                } else {
                    $text = $text[0];
                }
                $caption = $message['from'];
            }
        } else {
            $caption = $text[1] ?? '';
            $text = $text[0];
        }

        if ($message['format'] == 'location') {
            $text = $message['text'];
        }
        if (isset($old_message) and (is_null($message['mid3']) == false)) {
            $this->editMessageWithContent($channel['chid'],  $message['mid3'], $text, $message['format'], $caption);
        } else {
            $result = $this->sendMessageWithContent($channel['chid'], $text, $message['format'], $caption);
            Service::updateMessage(['mid3' => $result['result']['message_id']], $message['id']);
        }

        $this->deleteReply(null, $_session['in_edit']);

        if (isset($old_message)) {
            $this->deleteMessage($_channel->Id(), $_session->gd('in_edit'));
            $mid = $old_message['mid2'];
            $text = "\u{270F}".date('H:i', strtotime($message['dat_add'])).' '. $message['from']."\n";
        } else {
            $mid = $_session->gd('in_edit');
            $text = "\u{270F}".date('H:i', strtotime($message['dat_add'])).' '. $message['from']."\n";
        }

        if (in_array($before_edit_message['format'], [Message::STICKER, Message::LOCATION])) {
            $this->deleteMessage($_channel->Id(), $message['mid2']);

            if (Message::TEXT == $_message->Type()) {
                $result = $this->sendMessageWithContent($_channel->Id(), $text . $_message->Text(), $_message->Type(), $_message->Caption());
            } else {
                $result = $this->sendMessageWithContent($_channel->Id(), $_message->Text(), $_message->Type(), $text. $_message->Caption());
            }

            if (isset($result['result']['message_id'])) {
                Service::updateMessage(['mid2' => $result['result']['message_id']], $message['id']);
            }
        } else {
            if (Message::TEXT == $_message->Type()) {
                $this->editMessageWithContent($_channel->Id(),  $mid, $text . $_message->Text(), $_message->Type(), $_message->Caption());
            } else {
                $this->editMessageWithContent($_channel->Id(),  $mid, $_message->Text(), $_message->Type(), $text. $_message->Caption());
            }
        }

        $result = $this->send('_', ['reply_markup' => $this->getEmptyMenu()]);
        $this->delete();
        $this->deleteMessage($_channel->Id(), $result['result']['message_id']);
        $this->deleteArtifact();

        $this->nextMessage();
    }
}