<?php
namespace events;

use dto\Channel;
use dto\Message;
use dto\User;
use logics\DB;
use logics\DbConnection;
use logics\Handler;
use services\Service;
use telegram\Button;
use telegram\Config;
use telegram\InlineKeyboardBuilder;
use telegram\ReplyKeyboardBuilder;
use telegram\SessionData;


trait CommonEvent
{

    public function panelEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $result = $this->send('Главное меню', ['reply_markup'=>$this->getMenuByMode($_session, $_channel->Mode())]);
        $_session['artifact'] = $result['result']['message_id'];
    }

    private function sendToModerators(User $_user, Message $_message, $channel)
    {
        $message_db = Service::messageById($_message->Primary());

        if (in_array($message_db['status1'], [Handler::EDITED_MSG, Handler::EDITED_AFTER_MSG])) {
            $old_mid = $message_db['mid2'];
        } else {
            $old_mid = null;
        }

        $text = "\u{1F195}".date('H:i', strtotime($message_db['dat_add'])).' '.$message_db['from']."\n";

        if (Handler::EDITED_AFTER_MSG == $message_db['status1']) {
            if (!empty($channel['is_default'])) {
                if ($this->channel->isPrivate()) {
                    $text .= "изменил в личке\n";
                } else {
                    $text .= "изменил в группе ".$this->channel->Name()."\n";
                }
            }
        } else {
            if (!empty($channel['is_default'])) {
                if ($this->channel->isPrivate()) {
                    $text .= "написал в личке\n";
                } else {
                    $text .= "написал в группе ".$this->channel->Name()."\n";
                }
            }
        }

        if (Handler::EDITED_AFTER_MSG == $message_db['status1']) {

            $old_message = Service::originMessage($message_db['mid']);

            $text  .= "\u{270F} Изменено сообщение ";

            if (Message::TEXT === $_message->Type()) {

                if (Message::TEXT === $old_message['format']) {
                    $text .= 'Исходное: '.$old_message['text']."\n";
                } else {
                    $parts = explode('&#$', $old_message['text']);
                    $file  = $this->api->getFile($parts[0]);
                    $text .= "<a href='https://api.telegram.org/file/bot".BOT_TOKEN.'/'.$file['result']['file_path']. "'>Ссылка на исходник</a>\n";
                }
                $text .= 'Изменено: ';

            } else {

                if (Message::TEXT === $old_message['format']) {
                    $text .= 'Исходный текст: '.$old_message['text']."\n";
                } else {
                    $parts = explode('&#$', $old_message['text']);
                    $file  = $this->api->getFile($parts[0]);
                    $text .= "<a href='https://api.telegram.org/file/bot".BOT_TOKEN.'/'.$file['result']['file_path']. "'>Ссылка на исходник</a>\n";
                    $text .= 'Исходная подпись: ' . $parts[1]. ' ' ."\n";
                }
                $text .= 'Измененная подпись: ';

            }
        }

        if ($channel['is_bind']) {
            $reply = $this->getUnderMessageMenu($message_db['id']);
        } else {
            $reply = $this->getDefaultUnderMessageMenu($message_db['id']);
        }
        switch ($_message->Type()) {
            case Message::TEXT:
                if ($old_mid) {
                    $result = $this->editMessage(
                        $channel['moder_chid'],
                        $old_mid,
                        $text . $_message->Text(),
                        ['reply_markup' => $reply]
                    );
                } else {
                    $result = $this->sendMessage(
                        $channel['moder_chid'],
                        $text.$_message->Text(),
                        [ 'reply_markup' => $reply]
                    );
                }
                break;
            default:
                if ($old_mid) {
                    $result = $this->editMessageMedia(
                        $_message->Type(),
                        $channel['moder_chid'],
                        $old_mid,
                        $_message->Text(),
                        [
                            'caption' => $text . $_message->Caption(),
                            'reply_markup' => $reply
                        ]
                    );
                } else {
                    $result = $this->sendMessageMedia(
                        $_message->Type(),
                        $channel['moder_chid'],
                        $_message->Text(),
                        [
                            'caption' => $text.$_message->Caption(),
                            'reply_markup' => $reply
                        ]
                    );
                }
        }

        if ($result['ok'] == 1) {
            Service::updateMessage([
                'mid2' => $result['result']['message_id'],
                'sent' => 1
            ], $message_db['id']);
        }

    }

    public function editMessageWithContent($chat_id, $mid, string $text, string $type, string $caption = '')
    {
        switch ($type) {
            case Message::PHOTO:
            case Message::AUDIO:
            case Message::VIDEO:
            case Message::VOICE:
            case Message::DOC:
            case Message::ANIMATION:
            case Message::STICKER:
                return $this->editMessageMedia($type, $chat_id, $mid, $text, ['caption' => $caption]);
            case Message::LOCATION:
                return $this->editMessageMedia($type, $chat_id, $mid, $text.'&#$'. $caption);
            default:
                return $this->editMessage($chat_id, $mid, $text);
        }
    }

    public function sendMessageWithContent($chat_id, string $text, string $type, string $caption = '')
    {
        switch ($type) {
            case Message::PHOTO:
            case Message::AUDIO:
            case Message::VIDEO:
            case Message::VOICE:
            case Message::DOC:
            case Message::ANIMATION:
            case Message::STICKER:
                return $this->sendMessageMedia($type, $chat_id, $text, ['caption' => $caption]);
            case Message::LOCATION:
                return $this->sendMessageMedia($type, $chat_id, $text.'&#$'.$caption);
            default:
                return $this->sendMessage($chat_id, $text);
        }
    }

    public function allNonModeratedEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $this->messageFilter(0);
    }

    public function allModeratedEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $this->messageFilter(1);
    }

    public function allBlockedEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $this->messageFilter(2);
    }

    public function allEditedEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $this->messageFilter(3);
    }


    public function modMessageEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $this->check_reply = false;

        if ($_channel->isTester()) {
            $message = Service::testMessageById($_session->gd('cur_mid'));
        } else {
            $message = Service::messageById($_session->gd('cur_mid'));
        }

        $result = $this->send('_', ['reply_markup' => $this->getEmptyMenu()]);
        $this->deleteMessage($_channel->Id(), $result['result']['message_id']);

        if ($message['status1'] != 0) {
            return $this->send('Сообщение '.$message['text'].' уже отмодерировано');
        }

        $reply = $this->getUnderMessageMenu($message['id']);
        $this->editMessageReplyMarkup($_channel->Id(), $_session->gd('in_edit'), $reply);
    }

    public function domMessageEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $this->check_reply = false;

        $message = Service::messageById($_session->gd('cur_mid'));

        $result = $this->send('_', ['reply_markup' => $this->getEmptyMenu()]);
        $this->deleteMessage($_channel->Id(), $result['result']['message_id']);

        $reply = $this->getDefaultUnderMessageMenu($message['id']);
        $this->editMessageReplyMarkup($_channel->Id(), $_session->gd('in_edit'), $reply);
    }

    private function messageFilter(int $status1)
    {
        $no_message_text = [
            "\u{23F3}Немодерированных сообщений нет",
            "\u{2705}Модерированных сообщений нет",
            "\u{26D4}Заблокированных сообщений нет",
            "\u{270F}Измененных сообщений нет",
            "Сообщений нет",
        ];

        $message_text = [
            "\u{23F3}Немодерированные сообщения",
            "\u{2705}Модерированные сообщения",
            "\u{26D4}Заблокированные сообщения",
            "\u{270F}Измененные сообщения",
        ];

        $messages = Service::messagesByStatus($status1);

        if (empty($messages)) {
            $status1 = $status1 > 3? 0 : $status1;
            return $this->send($no_message_text[$status1]);
        }

        if (0 == $status1) {
            $this->send($message_text[$status1]);
            $text = "";
        } else {
            $text = $message_text[$status1]."\n";
        }


        foreach ($messages as $message) {
            $text .= $this->prepareMessage($message)."\n";

            if (0 == $status1) {
                $reply = InlineKeyboardBuilder::create()->add(Button::create('Открыть для модерации', '/mod_message', ['mid' => $message['id']]))->build();
                $this->send($text, ['reply_markup'=>$reply]);
                $text = "";
            }
        }
        if (!empty($text)) {
            $this->send($text);
        }
    }

    public function sendMessageEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $listeners = Service::listeners();

        foreach ($listeners as $listener) {
            $data[] = [
                'text' => empty($listener['fak_nam'])? $listener['name'] : $listener['fak_nam'],
                'cmd'  => '/listener',
                'data' => ['lsn' => $listener['uid']]
            ];
        }

        $reply_markup = ReplyKeyboardBuilder::create()->resize_able()
            ->add($data, 2)
            ->addButton(Button::create('На главную', ''))
            ->build();
        $this->switchToSelectionMode('listener');
        $this->send('Список пользователей', ['reply_markup'=>$reply_markup]);
    }

    public function listenerEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $listener = Service::listener($_session['lsn']);

        $result = $this->send("Введите сообщение ");

        $_session['input_wait'] = ['cmd' => '/em',  'm' => $result['result']['message_id'], 'mid' => $_message->Id()];
    }


    public function informListener($message_id, $sufix_text)
    {
        $msg = DbConnection::getInstance()
            ->query("SELECT m.*, c.`chid` FROM `messages` m INNER JOIN `channels` c ON (m.`channel_id` = c.`id`) WHERE m.id = :id")
            ->bind(':id', $message_id)
            ->one();

        $text = "Ваше сообщение ". substr($msg['text'],0, 50) ."... от " . date("d.m.y H:i", strtotime($msg['dat_add'])) .
            $sufix_text;
        $this->sendMessage($msg['chid'], $text);
    }



    public function editMessageStatus(Channel $channel, array $message)
    {
       if ($channel->isTester()) {
           Service::updateMessageForTest(['status1'=>$message['status1']], $message['id']);
       } else {
           Service::updateMessage(['status1'=>$message['status1']], $message['id']);
       }

    }
}