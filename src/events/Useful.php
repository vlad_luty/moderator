<?php

namespace events;


trait Useful
{
    public function prepareMessage(array $message)
    {
        $text = 'Время: '.date('d.m.Y H:i', strtotime($message['dat_add']))."\n".
            'От: '.$message['from']."\n".
            //'*К: *'.$message['to']."\n".
            'Текст: '.$message['text'];

        return $text;
    }

    public function switchToSelectionMode(string $handle_cmd)
    {
        $this->session['select_wait'] = $handle_cmd;
    }

    public function deleteArtifact()
    {
        if (isset($this->session['artifact'])) {
            $this->deleteMessage($this->channel->Id(), $this->session->gd('artifact'));
        }
    }

    public function deletePrev()
    {
        $message_id = $this->message->Id();
        $this->deleteMessage($this->channel->Id(), $message_id);
    }
}