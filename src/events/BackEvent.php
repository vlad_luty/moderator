<?php
namespace events;


use dto\Channel;
use dto\Message;
use dto\User;
use telegram\SessionData;

trait BackEvent
{
    public function backToPanelEvent()
    {
        switch ($this->user->Role()) {
            case Roles::WORKER:

                $this->delegate('issue');
                break;
            default:
                $this->delegate('panel', true);
        }
    }

}