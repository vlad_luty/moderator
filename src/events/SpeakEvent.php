<?php
namespace events;

use dto\Channel;
use dto\Message;
use dto\User;
use services\Service;
use telegram\Button;
use telegram\InlineKeyboardBuilder;
use telegram\ReplyKeyboardBuilder;
use telegram\SessionData;
use utils\DB;


trait SpeakEvent
{

    public function selectSpeakerEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
       /* if (isset($_session['speaker'])) {
            $user = Service::userByUid($_session['speaker']);
            $this->toSpeak($user['fak_nam'],$_channel->Id());
            return -1;
        }*/

        $channels = Service::supers();

        if (empty($channels[1])) {
           return $this->send('Список собеседников пуст');
        }

        foreach ($channels as $channel) {
            if ($channel['chid'] == $_channel->Id()) {
                continue;
            }
            $items[] = [
                'text' => $channel['name'],
                'cmd'  => 'speaker',
                'data' => ['speaker' => $channel['chid']]
            ];
        }
        $keyboard = ReplyKeyboardBuilder::create()->add($items, 2)->resize_able();

        $keyboard->addButton(Button::create('На главную', 'back_to_panel'));
        $this->switchToSelectionMode('speaker');
        $this->send('Выберите собеседника', ['reply_markup'=>$keyboard->build()]);
    }

    public function speakPauseEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        unset($_session['select_wait'], $_session['input_wait']);

        $speaker_session = new SessionData($_session['speaker']);
        $speaker_channel = Service::channelByChid($_session['speaker']);

        if (! isset($speaker_session['speaker'])) {
            $this->delegate('panel');
            return -1;
        }

        $this->send('Беседа приостановлена', ['reply_markup'=>$this->getMenuByMode($_session,$_channel->Mode())]);
        $this->sendMessage(
            $speaker_channel['chid'],
            'Собеседник приостановил беседу',
            $this->getMenuByMode($speaker_session, $speaker_channel['mode'])
        );
    }

    public function speakerEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        unset($_session['select_wait']);

        $speaker_channel = Service::channelByName($_message->rawtext);

        $speaker_channel_session = new SessionData($speaker_channel['chid']);

        $_session['speaker'] = $speaker_channel['chid'];

        $speaker_channel_session['speaker'] = $_channel->Id();

        $speaker_channel_session['input_wait'] = ['cmd' => 'in_speak', 'chid' => $speaker_channel['chid']];

        $speaker_channel_session->store();

        $_session['starter'] = 1;

        $this->delete();

        $reply_markup = ReplyKeyboardBuilder::create()
            ->add(Button::create('Завершить беседу', 'speak_end'))
            //->add(Button::create('Приостановить беседу', 'speak_pause'))
            ->resize_able()
            ->build();

        $text = 'По завершению общения нажмите на необходимый пункт меню';
        $result = $this->send($text, ['reply_markup' => $reply_markup]);
        $this->session['input_wait'] = ['cmd' => 'in_speak', 'chid' => $_channel->Id()];
    }

    public function denySpeakEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $speaker_session = new SessionData($_session['speaker']);

        $speaker_channel = Service::channelByChid($_session['speaker']);

        $this->sendMessage($_session['speaker'], 'Собеседник отклонил общение',['reply_markup'=>$this->getMenuByMode($speaker_session, $speaker_channel['mode'])]);

        $this->send('Главное меню', ['reply_markup'=>$this->getMenuByMode($_session, $_channel->Mode())]);

        unset($speaker_session['speaker'], $speaker_session['dialog_id'], $speaker_session['input_wait'], $speaker_session['starter']);
        unset($_session['speaker'], $_session['dialog_id'], $_session['input_wait'], $_session['starter']);

    }


    public function speakEndEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $speaker = $_session['speaker'];

        $session_speaker = new SessionData($speaker);

        if (isset($session_speaker['speaker'])) {
            $speaker_channel = Service::channelByChid($speaker);

            if(isset($session_speaker['speaker'])) {
                $this->sendMessage(
                    $speaker,
                    'Беседа завершена',
                    ['reply_markup' => $this->getMenuByMode($session_speaker, $speaker_channel['mode'])]
                );
            }
        }

        $this->send('Беседа завершена', ['reply_markup'=>$this->getMenuByMode($_session, $_channel->Mode())]);

        unset($session_speaker['speaker'], $session_speaker['dialog_id'], $session_speaker['input_wait'], $session_speaker['starter']);
        unset($_session['speaker'], $_session['dialog_id'], $_session['input_wait'], $_session['starter']);

        $session_speaker->store();
    }

    public function inSpeakEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        if (empty($_message->Text())){
            return -1;
        }
        $speaker_session = new SessionData($_session['speaker']);
        $speaker_user = Service::userByUid($_session['speaker']);

        $chenn = Service::channelByChid($_session['speaker']);

        $text = $chenn['name'] . ': ';

        if (empty($speaker_session['dialog_id'])) {
          //  $speaker_session['speaker'] = $_user->Id();

            $speaker_session['dialog_id'] = $_session['dialog_id'] = Service::startDialog();

            /*$reply_markup = ReplyKeyboardBuilder::create()->resize_able()
                ->add(Button::create('Ответить на беседу', 'accept_speak'))
                ->add(Button::create('Отклонить беседу', 'deny_speak'))
                ->build();*/

        }

        $reply_markup = ReplyKeyboardBuilder::create()
            ->add(Button::create('Завершить беседу', 'speak_end'))
           // ->add(Button::create('Приостановить беседу', 'speak_pause'))
            ->resize_able()
            ->build();

        switch ($_message->Type()) {
            case Message::TEXT:
                $this->sendMessage(
                    $_session['speaker'],
                    $text.$_message->Text(),
                    ['reply_markup' => $reply_markup]
                );
                break;
            default:
                $this->sendMessageMedia(
                    $_message->Type(),
                    $_session['speaker'],
                    $_message->Text(),[
                        'caption' => $text.$_message->Caption(),
                        'reply_markup' => $reply_markup
                    ]
                );
        }

        $speaker_session->store();

        Service::storeOriginDialog([
            'dialog_id' => $_session['dialog_id'],
            'from' => $_user->Primary(),
            'to'   => $speaker_user['id'],
            'text' => $_message->isText()? $_message->Text() : $_message->Text() . '&#$' . $_message->Caption(),
            'format' => $_message->Type()
        ]);

        return false;
    }
}