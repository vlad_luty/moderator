<?php
namespace events;

use dto\Channel;
use dto\Message;
use dto\User;
use logics\DB;
use logics\DbConnection;
use logics\Handler;
use services\Service;
use telegram\Button;
use telegram\Config;
use telegram\InlineKeyboardBuilder;
use telegram\ReplyKeyboardBuilder;
use telegram\SessionData;


trait TxtEvent
{
    public function exportTxtEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $moderators = Service::testers();

        $keyboard = InlineKeyboardBuilder::create();

        $files = scandir(FILES_DIR);
        foreach ($files as $file) {
            if ($file === '.' or $file === '..') {
                continue;
            }
            unlink(FILES_DIR.'/'.$file);
        }

        $dialog = -1;
        foreach ($moderators as $moderator) {

            $messages = Service::testMessagesByChannelId($moderator['id']);

            $fw = fopen(FILES_DIR.'/'.$moderator['id'].'.html', 'w');

            fwrite($fw, pack("CCC",0xef,0xbb,0xbf));

            $html = '
                <!DOCTYPE html>
                <html lang="ru">
                <head>
                    <meta charset="UTF-8">
                    <title>Title</title>
                    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
                </head>
                <body>
                <h2 class="text-center">'.$moderator['name'].'</h2>';

            fwrite($fw, $html);

            foreach ($messages as $message) {

                if ($dialog != $message['dialog']) {
                    $dialog = $message['dialog'];

                    if ($dialog != -1) {
                        $text = '
                            </tbody></table>
                            <table class="table table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Дата время</th>
                                    <th>От</th>
                                    <th>Для</th>
                                    <th>Текст</th>
                                    <th>Измененный текст</th>
                                    <th>Статус</th>
                                </tr>
                            </thead>
                            <tbody>';
                    } else {
                        $text = '<table class="table table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Дата время</th>
                                    <th>От</th>
                                    <th>Для</th>
                                    <th>Текст</th>
                                    <th>Измененный текст</th>
                                    <th>Статус</th>
                                </tr>
                            </thead>
                    <tbody>';
                    }
                } else {
                    $text = '<tr>';
                }

                $text.= '<td>'.date('d.m.y H:i', strtotime($message['dat_add'])).'</td>' .
                    '<td>'.$message['from'].'</td>'.
                    '<td>'.$message['to'].'</td>';

                switch ($message['origin_format']) {
                    case 'text' :
                        $text.= '<td>'.$message['origin_text'].'</td>';
                        break;
                    case 'sticker':
                        $text.= '<td>Стикер</td>';
                        break;
                    case 'location':
                        $text.= '<td>Геолокация</td>';
                        break;
                    case 'photo':
                        $img = explode('&#$',$message['origin_text']);
                        $file = $this->api->getFile($img[0]);
                        $file_path = 'https://api.telegram.org/file/bot'.BOT_TOKEN.'/'.$file['result']['file_path'];
                        $text.= '<td><img width="100" src="'.$file_path.'" /><br/>'.($img[1]??'').'</td>';
                        break;
                    default:
                        $img = explode('&#$',$message['origin_text']);
                        $file = $this->api->getFile($img[0]);
                        $file_path = 'https://api.telegram.org/file/bot'.BOT_TOKEN.'/'.$file['result']['file_path'];
                        $text.= '<td>'.'<a href="'.$file_path.'">Ссылка на файл '.$message['format'].'<br/>'.($img[1]??'').'</a></td>';
                }

                switch ($message['status1']) {
                    case 0:
                        $text .= '<td>'.'-'.'</td>';
                        $text .= '<td>'.'-'.'</td>';
                        break;
                    case 1:
                        $text .= '<td>'.'-'.'</td>';
                        $text .= '<td><b style="color: #2ed05a;">OK</b>'.'</td>';
                        break;
                    case 2:
                        $text .= '<td>'.'-'.'</td>';
                        $text .= '<td><b style="color: red;">Бан</b>'.'</td>';
                        break;
                    case 3:{

                        switch ($message['format']) {
                            case 'text' :
                                $text.= '<td>'.$message['text'].'</td>';
                                break;
                            case 'sticker':
                                $text.= '<td>Стикер</td>';
                                break;
                            case 'location':
                                $text.= '<td>Геолокация</td>';
                                break;
                            case 'photo':
                                $img = explode('&#$',$message['text']);
                                $file = $this->api->getFile($img[0]);
                                $file_path = 'https://api.telegram.org/file/bot'.BOT_TOKEN.'/'.$file['result']['file_path'];
                                $text.= '<td><img width="100" src="'.$file_path.'" /><br/>'.($img[1]??'').'</td>';
                                break;
                            default:
                                $img = explode('&#$',$message['text']);
                                $file = $this->api->getFile($img[0]);
                                $file_path = 'https://api.telegram.org/file/bot'.BOT_TOKEN.'/'.$file['result']['file_path'];
                                $text.= '<td>'.'<a href="'.$file_path.'">Ссылка на файл '.$message['format'].'<br/>'.($img[1]??'').'</a></td>';
                        }

                    }
                        $text .= '<td><b style="color: blue;">Изменено</b>'.'</td>';
                        break;
                }
                fwrite($fw,$text.'</tr>');
            }
            fwrite($fw, '</tbody></table></body></html>');
            fclose($fw);
            $keyboard->addButton(Button::create($moderator['name'], FILES.$moderator['id'].'.html')->asUrl());
        }

        $text = 'Ссылки на файлы'."\n";
        $this->send($text, ['reply_markup'=>$keyboard->build()]);
    }


}