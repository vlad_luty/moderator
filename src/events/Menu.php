<?php

namespace events;


use dto\Channel;
use telegram\Button;
use telegram\InlineKeyboardBuilder;
use telegram\ReplyKeyboardBuilder;
use telegram\SessionData;

trait Menu
{
    public function getUnderMessageMenuWithoutEdit(int $message_id)
    {
        return InlineKeyboardBuilder::create()
            ->addButton(Button::create("\u{2705} OK", 'ok', ['mid' => $message_id]))
            ->addButton(Button::create("БАН", 'block', ['mid' => $message_id]))
            ->build();
    }


    public function getUnderMessageMenu($message_id)
    {
        return InlineKeyboardBuilder::create()
            ->addButton(Button::create("\u{2705} OK", 'ok', ['mid' => $message_id]))
            ->addButton(Button::create("\u{26D4} БАН", 'block', ['mid' => $message_id]))
            ->addButton(Button::create("\u{270F} ИЗМЕНИТЬ", 'edit', ['mid' => $message_id]))
            ->build();
    }

    public function getDefaultUnderMessageMenu($message_id)
    {
       return InlineKeyboardBuilder::create()
            ->addButton(Button::create("\u{1F4AC} Ответить", 'reply', ['mid' => $message_id]))
            ->addButton(Button::create("\u{26D4} Игнорировать", 'block', ['mid' => $message_id]))
            ->build();
    }

    public function getSuperModeratorMenu()
    {
        return ReplyKeyboardBuilder::create()
            //->addButton(Button::create("Написать сообщение", ''))
            ->addButton(Button::create("Записать диалог", ''))
            ->addButton(Button::create('Отправить диалог', ''))
            ->addButton(Button::create("Файлы", ''))
            ->end()
            ->resize_able()
            ->build();
    }

    public function getOkReply()
    {
        return $this->getOkOrBlockOrEditReply("\u{2705}");
    }

    public function getBlockReply()
    {
        return $this->getOkOrBlockOrEditReply("\u{26D4}");
    }

    public function getEditReply()
    {
        return $this->getOkOrBlockOrEditReply("\u{270F}");
    }

    private function getOkOrBlockOrEditReply(string $emoji): string
    {
        return InlineKeyboardBuilder::create()
            ->add(Button::create($emoji, 'empty'))
            ->build();
    }

    public function getTesterMenu()
    {
        return $this->getEmptyMenu();
    }


    public function getModeratorMenu()
    {
        return ReplyKeyboardBuilder::create()
            //->add(Button::create("Написать сообщение", ''))
           // ->addButton(Button::create("\u{23F3} Немодерированные сообщения", ''))
           // ->addButton(Button::create("\u{2705} Модерированные сообщения", ''))
           // ->end()
           // ->addButton(Button::create("\u{26D4} Заблокированные сообщения", ''))
          //  ->addButton(Button::create("\u{270F} Измененные сообщения", ''))
          //  ->end()
            ->addButton(
                isset($this->session['notify'])?
                    Button::create("\u{1F515}Выключить уведомления", 'notify_off') :
                    Button::create("\u{1F514}Включить уведомления", 'notify_on')
            )
            ->resize_able()
            ->build();
    }

    public function getEmptyMenu()
    {
        return json_encode(array(
            'remove_keyboard' => true
        ));
    }

    public function getMenuByMode(SessionData $session, string $mode): ?string
    {
        switch ($mode) {
            case Channel::SUPER_MODE:
                return $this->getSuperModeratorMenu();
            default:
                return $this->getEmptyMenu();
        }
    }

}