<?php
namespace events;

use dto\Channel;
use dto\Message;
use dto\User;
use logics\DB;
use logics\DbConnection;
use logics\Handler;
use services\Service;
use telegram\Button;
use telegram\Config;
use telegram\InlineKeyboardBuilder;
use telegram\ReplyKeyboardBuilder;
use telegram\SessionData;


trait StartEvent
{
    public function startEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $switch_clause = $_session['ref_link'] ?? $_channel->Mode();
        unset($_session['ref_link']);
        $this->send(BOT_WELCOME);

        switch ($switch_clause) {
            case Channel::TESTER_MODE:
                $this->delegate('as_tester');
                return;
            case Channel::SUPER_MODE:
                $this->delegate('as_super');
                return;
            case Channel::MODERATOR_MODE:
                $this->delegate('as_moderator');
                return;
            case Channel::LISTENER_MODE:
                $this->delegate('as_listener');
                return;
        }
    }

    public function asTesterEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $_channel->setMode(Channel::TESTER_MODE);
        Service::updateChannel([
            'type' => $_channel->Type(),
            'mode' => $_channel->Mode()
        ], $_channel->Primary());

       // $this->send('Режим Модератор', ['reply_markup'=>$this->getTesterMenu()]);
    }

    public function asModeratorEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $_channel->setMode(Channel::MODERATOR_MODE);
        Service::updateChannel([
            'type' => $_channel->Type(),
            'mode' => $_channel->Mode()
        ], $_channel->Primary());

       // $this->send('Режим *Модератор*', ['reply_markup'=>$this->getEmptyMenu()]);
    }

    public function asDeveloperEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $_channel->setMode(Channel::LISTENER_MODE);
        Service::updateChannel([
            'type' => $_channel->Type(),
            'mode' => $_channel->Mode()
        ], $_channel->Primary());

        DB::get()->update('bots', ['channel_id' => $_channel->Primary()], ['id' => BOT_ID]);

      //  $this->send('Режим *Слушатель*');
    }

    public function asClientEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $this->delegate('as_listener');
    }

    public function asListenerEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $_channel->setMode(Channel::LISTENER_MODE);
        Service::updateChannel([
            'type' => $_channel->Type(),
            'mode' => $_channel->Mode()
        ], $_channel->Primary());
       // $this->send('Режим *Слушатель*');
    }

    public function asSuperEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        $_channel->setMode(Channel::SUPER_MODE);
        Service::updateChannel([
            'type' => $_channel->Type(),
            'mode' => $_channel->Mode()
        ], $_channel->Primary());
        $this->send('Режим Супер модератор', ['reply_markup' => $this->getSuperModeratorMenu()]);
    }

    public function notifyOnEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        /*$_session['notify'] = 1;
        $this->send('*Внимание!* Все слушатели будут автоматически оповещены о результате модерации их сообщений');
        $this->delegate('panel');*/
    }

    public function notifyOffEvent(User $_user, Message $_message, Channel $_channel, SessionData $_session)
    {
        unset($_session['notify']);
        $this->send('Оповещение слушателей выключено');
        $this->delegate('panel');
    }
}