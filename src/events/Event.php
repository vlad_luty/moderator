<?php
namespace events;


trait Event
{
    use Useful,
        CancelEvent,
        CommonEvent,
        TxtEvent,
        TesterEvent,
        BackEvent,
        ButtonsTrait,
        ModerateEvent,
        TestModerateEvent,
        SpeakEvent,
        Menu,
        StartEvent;
}