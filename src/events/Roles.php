<?php
namespace events;


interface Roles
{
    const WORKER   = 'worker';
    const TEAMLEAD = 'teamlead';
    const CLIENT   = 'client';
}