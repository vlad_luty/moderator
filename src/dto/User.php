<?php
namespace dto;


class User
{
    private $primary;
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $nam;
    /**
     * @var string
     */
    private $username = '';
    /**
     * @var string
     */
    private $fake;
    /**
     * @var string
     */
    private $fio;
    /**
     * @var string
     */
    private $role;
    /**
     * @return mixed
     */

    public function Primary()
    {
        return $this->primary;
    }

    /**
     * @param mixed $primary
     */
    public function setPrimary($primary): void
    {
        $this->primary = $primary;
    }

    public function Role()
    {
        return $this->role;
    }

    public function setRole(string $role)
    {
        $this->role = $role;
    }

    public function Nam()
    {
        return $this->nam;
    }

    public function setNam(string $nam)
    {
        $this->nam = $nam;
    }

    /**
     * @return string
     */
    public function Id(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function Username(): string
    {
        if(empty($this->username)) {
            return '';
        } else {
            return '@' . $this->username;
        }
    }

    /**
     * @param string $username
     */
    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function Fio(): string
    {
        return $this->fio;
    }

    /**
     * @param string $fio
     */
    public function setFio(string $fio): void
    {
        $this->fio = $fio;
    }

    public static function create
    (
        string $id,
        ?string $username,
        string $fio
    ): self {
        $user = new self;
        $user->setId($id);
        $user->setUsername($username);
        $user->setFio($fio);
        return $user;
    }
}