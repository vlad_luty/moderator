<?php
namespace dto;


use helpers\ArrayOrJsonTrait;
use logics\DbConnection;

class Channel
{
    const CHANNEL     = 'channel';
    const GROUP       = 'group';
    const SUPERGROUP  = 'supergroup';
    const PRIVATE     = 'private';

    const TESTER_MODE       = 'test';
    const LISTENER_MODE     = 'listener';
    const MODERATOR_MODE    = 'moderator';
    const DEFMODERATOR_MODE = 'defmoderator';
    const SUPER_MODE        = 'super';

    private $primary;
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $type;

    private $mode = self::LISTENER_MODE;

    /**
     * @return mixed
     */
    public function Primary()
    {
        return $this->primary;
    }

    /**
     * @param mixed $primary
     */
    public function setPrimary($primary): self
    {
        $this->primary = $primary;
        return $this;
    }



    public function fields(): array
    {
        return [
            'chid'  => $this->id,
            'type'  => $this->type,
            'nam'   => $this->type . $this->id,
            'title' => $this->name
        ];
    }

    /**
     * @return string
     */
    public function Id(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function Name(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function Type(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function Mode(): string
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode(string $mode): self
    {
        $this->mode = $mode;
        return $this;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function isChannel(): bool
    {
        return $this->type === self::CHANNEL;
    }

    public function isGroup(): bool
    {
        return ($this->type === self::GROUP) or ($this->type === self::SUPERGROUP);
    }

    public function isPrivate(): bool
    {
        return ($this->isGroup() === false) and ($this->isChannel() === false);
    }

    public function isTester(): bool
    {
        return self::TESTER_MODE == $this->mode;
    }

    public function isModerator(): bool
    {
        return self::MODERATOR_MODE == $this->mode or self::DEFMODERATOR_MODE == $this->mode;
    }

    public function isListener(): bool
    {
        return self::LISTENER_MODE == $this->mode;
    }

    public function isSuper(): bool
    {
        return self::SUPER_MODE == $this->mode;
    }

    public static function create(
        string $id,
        string $name,
        string $type = self::PRIVATE
    ): Channel {
        $channel = new self();
        $channel->setId($id);
        $channel->setName($name);
        $channel->setType($type);
        return $channel;
    }

}