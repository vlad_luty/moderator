<?php
namespace dto;


final class Message implements \ArrayAccess
{
    const REGEX = '/^(?:@\w+\s)?\/([^\s@]+)(@\S+)?\s?(.*)$/';

    const PHOTO = 'photo';
    const VIDEO = 'video';
    const VOICE = 'voice';
    const AUDIO = 'audio';
    const TEXT  = 'text';
    const DOC   = 'document';
    const ANIMATION   = 'animation';
    const LOCATION    = 'location';
    const STICKER    = 'sticker';

    private $id;

    /**
     * @var string
     */
    private $caption = '';
    /**
     * @var string
     */
    private $type = self::TEXT;

    /**
     * @var int
     */
    private $primary;

    /**
     * @var string
     */
    private $text;

    /**
     * @var int
     */
    private $date;

    /**
     * @var string
     */
    public $rawtext;
    /**
     * @var bool
     */
    private $edited;

    /**
     * @var array
     */
    private $options = [];

    /**
     * @var bool
     */
    private $command = false;
    /**
     * @var bool
     */
    private $converted = false;

    private $plain_command = [
        'отмена'        => 'cancel',
        'немодерированные сообщения' => '/all_non_moderated',
        'заблокированные сообщения'  => '/all_blocked',
        'модерированные сообщения'   => '/all_moderated',
        'измененные сообщения'       => '/all_edited',
        'написать сообщение'         => '/send_message',
        'записать диалог'            => '/select_speaker',

        'отправить диалог'            => '/send_dialog',

        'пообщаться'                 => '/select_speaker',

        'завершить беседу'           => '/speak_end',

        'приостановить беседу'       => '/speak_pause',

        'ответить на беседу'         => '/accept_speak',
        'отклонить беседу'           => '/deny_speak',

        'взять диалог в обработку'   => '/take_test',

        'изменить' => 'edit',

        'отмена'        => 'cancel',

        'файлы' => 'export_txt',

        'выключить уведомления' => '/notify_off',
        'включить уведомления'  => '/notify_on',


        'главная'               => '/panel',
        'на главную'            => '/panel',
        'открыть для модерации' => '/mod_message'
    ];

    public function Type(): string
    {
        return $this->type;
    }

    public function isDocument(): bool
    {
        return self::DOC === $this->type;
    }

    public function isPhoto(): bool
    {
        return self::PHOTO === $this->type;
    }

    public function isVideo(): bool
    {
        return self::VIDEO === $this->type;
    }

    public function isVoice(): bool
    {
        return self::VOICE === $this->type;
    }

    public function isAudio(): bool
    {
        return self::AUDIO === $this->type;
    }

    public function isAnimation(): bool
    {
        return self::ANIMATION === $this->type;
    }

    public function isText() : bool
    {
        return self::TEXT === $this->type;
    }

    public function Time(): string
    {
        return date('H:i', $this->date);
    }

    public function Date(): string
    {
        return date('Y-m-d', $this->date);
    }

    public function Datetime():string
    {
        return date('Y-m-d H:i:s', $this->date);
    }

    public function ShortDatetime():string
    {
        return date('d.m.y H:i:s', $this->date);
    }

    public function Caption(): string
    {
        return $this->caption;
    }

    /**
     * @return bool
     */
    public function isConverted(): bool
    {
        return $this->converted;
    }


    public function Primary(): int
    {
        return $this->primary;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function setCaption(string $caption): void
    {
        $this->caption = $caption;
    }

    public function setPrimary($primary): void
    {
        $this->primary = $primary;
    }

    public function Id(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function Text(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function isEdited(): bool
    {
        return $this->edited;
    }

    public function setEdited(bool $edited): void
    {
        $this->edited = $edited;
    }

    public function isCommand(): bool
    {
        return $this->command;
    }

    public function isSingleText(): bool
    {
        return $this->isCommand() == false;
    }

    public function setOptions(array $options): void
    {
        $this->options = $options;
    }

    public function Options():array
    {
        return $this->options;
    }

    private function parse($text): array
    {
        $options = json_decode($text, true);

        if (is_array($options)) {
            return $options;
        } elseif (preg_match(self::REGEX, $text)) {
            $parts = explode(' ', $text);

            $this->offsetSet('cmd', str_replace('/', '', $parts[0]));

            if (isset($parts[1])) {
                $this->offsetSet('ref_link', $parts[1]);
            }

            return $this->options;
        } else {
            return array();
        }
    }

    public function setCommand(bool $command)
    {
        $this->command = $command;
    }

    public function setConverted(bool $converted)
    {
        $this->converted = $converted;
    }

    public static function createMedia(
        string $id,
        $date,
        string $file_id,
        bool $edited = false
    ): Message {
        $message = new self();
        $message->setId($id);
        $message->setEdited($edited);


        $message->rawtext = $file_id;
        $message->text    = $file_id;

        $message->date = $date;

        return $message;
    }

    public static function create(
        string $id,
        $date,
        string $text,
        bool $edited = false
    ): Message {
        $message = new self();
        $message->setId($id);
        $message->setEdited($edited);

        $message->rawtext = $text;

        $clear_text = mb_strtolower(trim($text));
       // $clear_text = self::removeEmoji($text);

        if (isset($message->plain_command[$clear_text])) {
            $message->command = true;
            $message->converted = true;
            $text = $message->plain_command[$clear_text];
        }

        $options = $message->parse($text);

        $message->setOptions($options);

        if(!empty($options)) {
            $message->command = true;
            $message->setText($options['cmd']);
        } else {
            $message->setText($text);
        }
        $message->date = $date;

        return $message;
    }


    private static function removeEmoji(string $string)
    {
        $clear_string = preg_replace ("/[^«a-zA-ZА-Яа-я\d\s]/ui","",$string);
        return mb_strtolower(trim($clear_string));
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->options[] = $value;
        } else {
            $this->options[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->options[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->options[$offset]);
    }

    public function offsetGet($offset) {
        return $this->options[$offset] ?? null;
    }
}