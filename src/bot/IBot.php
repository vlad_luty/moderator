<?php

namespace bot;


interface IBot
{
    /**
     * Инфо о пользователе канала/чата/группы
     * @return mixed
     */
    public function getUser(): \dto\User;
    /**
     * Инфо о канале/чате/группе
     * @return mixed
     */
    public function getChannel(): \dto\Channel;
    /**
     * Инфо о сообщении
     * @return mixed
     */
    public function getMessage(): \dto\Message;
    /**
     * Полное инфо о сообщении
     * @return mixed
     */
    public function getData();
    /**
     * Является ли сообщение запросом боту
     * @return bool
     */
    public function isCommand(): bool;
    /**
     * Является ли сообщение простым текстом
     * @return bool
     */
    public function isSingleText(): bool;
    /**
     * Является ли чат личкой
     * @return bool
     */
    public function isPrivate(): bool;
    /**
     * Является ли чат групповым чатом
     * @return bool
     */
    public function isGroup(): bool;

    public function handle();

}