<?php
namespace services;

use dto\Channel;
use logics\DbConnection as DB;
use logics\DB           as EasyDb;

class Service
{
    private static function byMode($status)
    {
        return EasyDb::get()->run(
            "SELECT u.*, c.id as `channel_id`, c.chid, c.name, c.mode, c.type FROM `channels` c ".
            "LEFT JOIN `users` u ON(c.chid = u.uid) ".
            "WHERE c.`mode`=? AND c.`bot_id`=?",
            $status,
            $_REQUEST['bot']
        );
    }

    public static function testers()
    {
        return EasyDb::get()->run(
            "SELECT c.* FROM `channels` c ".
            "WHERE c.`mode`=? AND c.`bot_id`=?",
            Channel::TESTER_MODE,
            $_REQUEST['bot']
        );
    }

    public static function listeners()
    {
        return self::byMode(Channel::LISTENER_MODE);
    }

    public static function moderators()
    {
        return self::byMode(Channel::MODERATOR_MODE);
    }

    public static function supers()
    {
        return EasyDb::get()->run(
            "SELECT c.* FROM `channels` c ".
            "WHERE c.`mode`=? AND c.`bot_id`=?",
            Channel::SUPER_MODE,
            $_REQUEST['bot']
        );

        return self::byMode(Channel::SUPER_MODE);
    }

    public static function updateChannel(array $data, $id)
    {
        EasyDb::get()->update('channels',$data,
            ['id' => $id, 'bot_id' => $_REQUEST['bot']]);
    }

    public static function defaultModerId()
    {
        return EasyDb::get()->cell("SELECT `id` FROM `channels` WHERE `bot_id` = ? AND `mode`='defmoderator'", $_REQUEST['bot']);
    }

    public static function defaultModerChid()
    {
        return EasyDb::get()->cell("SELECT `chid` FROM `channels` WHERE `bot_id` = ? AND `mode`='defmoderator'", $_REQUEST['bot']);
    }

    public static function channelById($id)
    {
        return EasyDb::get()->row("SELECT * FROM `channels` WHERE `id`=? AND `bot_id`=?", $id, $_REQUEST['bot']);
    }

    public static function channelByName($name)
    {
        return EasyDb::get()->row("SELECT * FROM `channels` WHERE `name`=? AND `bot_id`=?", $name, $_REQUEST['bot']);
    }

    public static function bindingChannel(int $id)
    {
        return EasyDb::get()->run("SELECT * FROM `channel_to_channel` " .
            "WHERE `bot_id`=? AND (`client_id`=? OR `developer_id`=? OR `moder_id`=?)",
            $_REQUEST['bot'], $id, $id, $id
        );
    }

    public static function boundChannel(int $id)
    {
        return EasyDb::get()->row("SELECT IF(cc.client_id=?, c2.chid, c1.chid) as `chid`, IF(cc.client_id=?, 'client', 'developer') as `role`, cc.moder_id, c3.`chid` as `moder_chid`, ".
            "IF(c3.`mode`='defmoderator','1','0') as `is_default` ".
            "FROM `channel_to_channel` cc ".
            "INNER JOIN `channels` c1 ON(cc.client_id = c1.id AND cc.bot_id = c1.bot_id) ".
            "INNER JOIN `channels` c2 ON(cc.developer_id = c2.id AND cc.bot_id = c2.bot_id) ".
            "INNER JOIN `channels` c3 ON(cc.moder_id = c3.id AND cc.bot_id = c3.bot_id) ".
            "WHERE cc.`bot_id` = ? AND (cc.`client_id` = ? OR cc.`developer_id`=?)", $id, $id, BOT_ID, $id, $id);
    }

    public static function channelByChid($chid)
    {
        return EasyDb::get()->row("SELECT * FROM `channels` WHERE `chid`=? AND `bot_id`=?", $chid, $_REQUEST['bot']);
    }

    public static function testMessageNotModerated($channel_id)
    {
        return EasyDb::get()->row(
            "SELECT t.*, c.name, c.chid FROM `test` t INNER JOIN `channels` c ON (t.channel_id = c.id) ".
            "WHERE t.channel_id = ? AND t.status1 = 0 ORDER BY t.id ASC",
            $channel_id
        );
    }

    public static function userByFakNam(string $fak_nam)
    {
        return EasyDb::get()->row(
            "SELECT u.*, c.id as `channel_id`, c.chid, c.mode, c.type FROM `users` u ".
            "INNER JOIN `channels` c ON(u.uid = c.chid) ".
            "WHERE u.`fak_nam` = ? AND c.`bot_id`=?",
            $fak_nam,
            $_REQUEST['bot']);
    }

    public static function userById($id)
    {
        return EasyDb::get()->row(
            "SELECT u.*, c.id as `channel_id`, c.chid, c.mode, c.type FROM `users` u ".
            "INNER JOIN `channels` c ON(u.uid = c.chid) ".
            "WHERE u.`id` = ? AND c.`bot_id`=?",
            $id,
            $_REQUEST['bot']);
    }

    public static function userByUid($uid)
    {
        return EasyDb::get()->row(
            "SELECT u.*, c.id as `channel_id`, c.chid, c.mode, c.type FROM `users` u ".
            "INNER JOIN `channels` c ON(u.uid = c.chid) ".
            "WHERE u.`uid` = ? AND c.`bot_id`=?",
            $uid,
            $_REQUEST['bot']);
    }

    public static function users(array $data = [])
    {
        $where = [];
        $sql = "SELECT * FROM `users` WHERE 1 ";

        if (isset($data['uid'])) {
            $sql .= " AND `uid` != " . $data['uid'];
            $where['uid'] = $data['uid'];
        }

        DB::getInstance()->query($sql);

        foreach ($where as $k => $v) {
            DB::getInstance()->bind(':'.$k, $v);
        }
        return DB::getInstance()->all();
    }

    public static function listener($uid)
    {
        return EasyDb::get()->row(
            "SELECT u.* FROM `users` u ".
            "INNER JOIN `channels` c ON(u.uid = c.chid) ".
            "WHERE c.`mode`=? AND u.`uid`=? AND c.`bot_id`=?",
            Channel::LISTENER_MODE,
            $uid,
            $_REQUEST['bot']
        );
    }

    public static function dialogs()
    {
        return EasyDb::get()->run("SELECT dl.* FROM `dialog` d INNER JOIN " .
            "dialog_line dl ON (d.id = dl.dialog_id) GROUP BY d.id");
    }

    public static function startDialog()
    {
        DB::getInstance()
            ->query("INSERT INTO `dialog`(`status1`) VALUES (0)")
            ->execute();
        return DB::getInstance()->lastInsertId();
    }

    public static function storeOriginDialog(array $data)
    {
        EasyDb::get()->insert('dialog_line', $data);
        return EasyDb::get()->lastInsertId();
    }


    public static function storeMessageForTest(array $data)
    {
        EasyDb::get()->insert('test', $data);
        return EasyDb::get()->lastInsertId();
    }

    public static function updateMessageWithMid(array $data, int $mid)
    {
        EasyDb::get()->update('messages', $data, ['mid' => $mid]);
    }

    public static function updateMessage(array $data, int $message_id)
    {
        EasyDb::get()->update('messages', $data, ['id' => $message_id]);
        return self::messageById($message_id);
    }

    public static function updateMessageForTest(array $data, int $message_id)
    {
        EasyDb::get()->update('test', $data, ['id' => $message_id]);
        return self::testMessageById($message_id);
    }

    public static function messageIdByUid(int $mid)
    {
        return EasyDb::get()->cell("SELECT `id` FROM `messages` WHERE `mid`=?", $mid);
    }

    public static function channelName($id)
    {
        return EasyDb::get()->cell("SELECT `name` FROM `channels` WHERE `id`=?",  $id);
    }

    public static function nearNotSentMessage($id)
    {
        return EasyDb::get()->row("SELECT * FROM `messages` WHERE `sent` = 0 AND `status1` IN (6,0,4) AND `moder_id` = ? AND `bot_id`=? ORDER BY FIELD(`status1`, 6, 0, 4), `dat_add` ASC", $id, $_REQUEST['bot']);
    }

    public static function notModeratedMessages($id)
    {
        return EasyDb::get()->run("SELECT m.* FROM `messages` m ".
            "WHERE m.`status1` IN (0,4,6) AND m.`moder_id` = ? AND `bot_id`=? ORDER BY `dat_add`", $id, $_REQUEST['bot']);
    }

    public static function notSentMessages()
    {
        return EasyDb::get()->run("SELECT * FROM `messages` WHERE `sent` = 0");
    }

    public static function deleteTestById(int $id)
    {
        EasyDb::get()->delete('test', ['id' => $id]);
    }

    public static function deleteMessageById(int $id)
    {
        EasyDb::get()->delete('messages', ['id' => $id]);
    }

    public static function lastCondidat(int $mid)
    {
        return EasyDb::get()->row("SELECT m.*, u.uid, u.fio, u.nam, u.fak_nam as `from`, ch.chid, ch.type, ch.mode FROM `messages` m ".
            "INNER JOIN `channels` ch ON(m.channel_id = ch.id) ".
            "INNER JOIN `users` u ON(m.user_id = u.id) ".
            "WHERE m.mid = ? AND ch.`bot_id` = ? AND m.status1 = 6 ".
            "ORDER BY `dat_add` ASC",
            $mid,
            $_REQUEST['bot']);
    }

    public static function originMessage(int $mid)
    {
        return EasyDb::get()->row("SELECT m.*, u.uid, u.fio, u.nam, u.fak_nam as `from`, ch.chid, ch.type, ch.mode FROM `messages` m ".
            "INNER JOIN `channels` ch ON(m.channel_id = ch.id) ".
            "INNER JOIN `users` u ON(m.user_id = u.id) ".
            "WHERE m.mid = ? AND ch.`bot_id` = ? ".
            "ORDER BY `dat_add` ASC",
            $mid,
            $_REQUEST['bot']
        );
    }


    public static function messageByMid(int $mid)
    {
        return EasyDb::get()->row("SELECT m.*, u.uid, u.fio, u.nam, u.fak_nam as `from`, ch.chid, ch.type, ch.mode FROM `messages` m ".
            "INNER JOIN `channels` ch ON(m.channel_id = ch.id) ".
            "INNER JOIN `users` u ON(m.user_id = u.id) ".
            "WHERE (m.mid = ? OR m.mid2 = ? OR m.mid3 = ?) AND ch.`bot_id` = ? ".
            "ORDER BY `dat_add` ASC",
            $mid, $mid, $mid,
            $_REQUEST['bot']);
    }

    public static function messageById(int $id)
    {
        return EasyDb::get()->row("SELECT m.*, u.id as `user_id`, u.uid, u.fio, u.nam, u.fak_nam as `from`, ch.id as `channel_id`, ch.chid, ch.type, ch.mode FROM `messages` m ".
            "INNER JOIN `channels` ch ON(m.channel_id = ch.id) ".
            "INNER JOIN `users` u ON(m.user_id = u.id) ".
            "WHERE m.id = ? AND ch.`bot_id` = ?",
            $id,
            $_REQUEST['bot']);
    }

    public static function testMessageById(int $id)
    {
        return DB::getInstance()
            ->query("SELECT t.*, u1.`fak_nam` as `from`, u2.`fak_nam` as `to`, dl.`dat_add` as `dat_add` FROM `test` t INNER JOIN `dialog_line` dl ON (t.`origin_id` = dl.`id`) ".
                "INNER JOIN  `users` u1 ON(dl.`from`=u1.`id`) INNER JOIN `users` u2 ON(dl.`to`=u2.`id`) ".
                "WHERE t.id = :id")
            ->bind(':id', $id)
            ->one();
    }

    public static function testMessagesByChannelId(int $channel_id)
    {
        return DB::getInstance()
            ->query("SELECT t.*, u1.`fak_nam` as `from`, u2.`fak_nam` as `to`, dl.`dat_add` as `dat_add`, dl.text as `origin_text`, dl.format as `origin_format`, dl.dialog_id as `dialog` FROM `test` t INNER JOIN `dialog_line` dl ON (t.`origin_id` = dl.`id`) ".
                "INNER JOIN  `users` u1 ON(dl.`from`=u1.`id`) INNER JOIN `users` u2 ON(dl.`to`=u2.`id`) ".
                "WHERE t.channel_id = :user_id")
            ->bind(':user_id', $channel_id)
            ->all();
    }

    public static function dialogById(int $dialog_id)
    {
        return EasyDb::get()->run("SELECT dl.*, u1.`fak_nam` as `from`, u2.`fak_nam` as `to` FROM `dialog_line` dl INNER JOIN  `users` u1 ON(dl.`from`=u1.`id`) INNER JOIN `users` u2 ON(dl.`to`=u2.`id`) " .
            "WHERE dl.`dialog_id` = ?", $dialog_id);
    }

    public static function messagesByStatus(int $status)
    {
        return DB::getInstance()
            ->query("SELECT m.*, u.`fak_nam` as `from` FROM `messages` m " .
                "INNER JOIN `users` u ON(m.`user_id`=u.`id`) ".
                "WHERE m.`status1`=:status")
            ->bind(':status', $status)
            ->all();
    }

    public static function testMessagesByStatus(int $status)
    {
        return DB::getInstance()
            ->query("SELECT t.*, u1.`fak_nam` as `from`, u2.`fak_nam` as `to`, dl.`dat_add` as `dat_add` FROM `test` t INNER JOIN `dialog_line` dl ON (t.`origin_id` = dl.`id`) ".
                "INNER JOIN  `users` u1 ON(dl.`from`=u1.`id`) INNER JOIN `users` u2 ON(dl.`to`=u2.`id`) ".
                "WHERE t.`status1` = :status")
            ->bind(':status', $status)
            ->all();
    }

    public static function storeMessage(array $data)
    {
        EasyDb::get()->insert('messages', $data);
        return EasyDb::get()->lastInsertId();
    }


}