<?php

namespace helpers;

trait GetTrait
{
    function __get($name)
    {
        if(method_exists($this, $name)){
            return $this->$name();
        } elseif(property_exists($this,$name)){
            return $this->$name;
        }
        return null;
    }
}