<?php

namespace helpers;

trait SetTrait
{
    function __set($name,$value)
    {
        if(method_exists($this, $name)){
            $this->$name($value);
        } else{
            $this->$name = $value;
        }
    }

    function fromArray(array $data)
    {
        foreach ($data as $k => $v) {
            @$this->$k = $v;
        }
    }
}