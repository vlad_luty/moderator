<?php

namespace utils;

class Log {

    private const LOG_FILE_DEFAULT = 'log.txt';
    /**
     * $log_file - path and log file name
     * @var string
     */
    protected $log_file;
    /**
     * $file - file
     * @var string
     */
    protected $file;
    /**
     * $options - settable options - future use - passed through constructor
     * @var array
     */
    protected $options = array(
        'dateFormat' => 'Y-m-d H:i:s'
    );

    /**
     * @var bool
     */
    protected $pretty = false;

    /**
     * @var \utils\Log
     */
    protected static $instance;

    /**
     * Class constructor
     * @param string $log_file - path and filename of log
     * @param array $params
     */
    private function __construct($log_file = self::LOG_FILE_DEFAULT, $params = array()){
        $this->log_file = DIR .'/logs/'.$log_file;
        $this->params = array_merge($this->options, $params);

        if(!file_exists($this->log_file)){
            $this->file = @fopen($this->log_file, 'a+');
        }
    }
    /**
     * Info method (write info message) with pretty output
     * @param string $message
     * @return void
     */
    public static function prettyInfo($message, string $file = self::LOG_FILE_DEFAULT)
    {
        static::get($file)->writeLog($message, 'INFO', true);
    }
    /**
     * Info method (write info message)
     * @param string $message
     * @return void
     */
    public static function info($message, string $file = self::LOG_FILE_DEFAULT)
    {
        static::get($file)->writeLog($message, 'INFO');
    }
    /**
     * Debug method (write debug message)
     * @param string $message
     * @return void
     */
    public static function debug($message, string $file = self::LOG_FILE_DEFAULT)
    {
        if(DEBUG_MODE){
            static::get($file)->writeLog($message, 'DEBUG');
        }
    }
    /**
     * Warning method (write warning message)
     * @param string $message
     * @return void
     */
    public static function warning($message, string $file = self::LOG_FILE_DEFAULT)
    {
        static::get($file)->writeLog($message, 'WARNING');
    }
    /**
     * Error method (write error message)
     * @param string $message
     * @return void
     */
    public static function error($message, string $file = self::LOG_FILE_DEFAULT)
    {
        static::get($file)->writeLog($message, 'ERROR');
    }
    /**
     * Write to log file
     * @param string $message
     * @param string $severity
     * @return void
     */
    private function writeLog($message, $severity, $pretty = false) {
        // open log file

        static::$instance->openLog();

        // grab the url path ( for troubleshooting )
        $path = $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        //Grab time - based on timezone in php.ini
        $time = date(static::$instance->params['dateFormat']);
        // Write time, url, & message to end of file
        if($pretty && (is_array($message) || is_object($message))){
            fwrite(static::$instance->file, "[$time] [$path] : [$severity] - " . print_r($message, true) . PHP_EOL);
        }else{
            fwrite(static::$instance->file, "[$time] [$path] : [$severity] - $message" . PHP_EOL);
        }

    }
    /**
     * Open log file
     * @return void
     */
    private function openLog(){
        $openFile = $this->log_file;
        // 'a' option = place pointer at end of file
        $this->file = fopen($openFile, 'a+') or exit("Can't open $openFile!");
    }
    /**
     * Class destructor
     */
    public function __destruct(){
        if ($this->file) {
            fclose(static::$instance->file);
        }
    }

    private static function get(string $file = self::LOG_FILE_DEFAULT) : self
    {

        if(null == static::$instance){
            static::$instance = new static($file);
        }
        static::$instance->file = @fopen(dirname(__DIR__).'/logs/'.$file, 'a+');
        return static::$instance;
    }
}