<?php

include_once __DIR__ . '/index.php';


$bot = \logics\DB::get()->row("SELECT * FROM `bots` WHERE `id`=?", $_GET['bot']);

if (empty($bot)) {
    die();
}

define('BOT_ID',  $bot['id']);
define('BOT_NAME',  $bot['nam']);
define('BOT_WELCOME',  $bot['welcom']);
define('BOT_TOKEN',  $bot['token']);

$bot = new telegram\TelegramBot($bot['token']);

$handler = new logics\Handler(
    $bot
);
$handler->webhook();