<?php


define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'moderation');

define('HOST', 'https://4b5c87a5.ngrok.io/moderator');

define('API_BOT_HOOK', HOST.'/hook.php');

define('FILES', HOST.'/files/');

define('DEBUG_MODE', true);

