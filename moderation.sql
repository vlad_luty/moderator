/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : moderation

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-01-15 20:23:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bots
-- ----------------------------
DROP TABLE IF EXISTS `bots`;
CREATE TABLE `bots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nam` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `welcom` varchar(255) NOT NULL,
  `hook` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for channels
-- ----------------------------
DROP TABLE IF EXISTS `channels`;
CREATE TABLE `channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bot_id` int(11) NOT NULL,
  `chid` varchar(30) NOT NULL COMMENT 'внутрителеграммовский id канала',
  `name` varchar(255) DEFAULT NULL COMMENT 'наименование канала',
  `type` enum('private','group','supergroup') NOT NULL DEFAULT 'private' COMMENT 'тип канала: личка, группа, супергруппа',
  `mode` enum('listener','moderator','super','test','off','defmoderator') NOT NULL DEFAULT 'listener' COMMENT 'режим канала: слушатель, модератор, модератор по умолчанию',
  PRIMARY KEY (`id`),
  KEY `bot_id` (`bot_id`),
  CONSTRAINT `channels_ibfk_1` FOREIGN KEY (`bot_id`) REFERENCES `bots` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for channel_to_channel
-- ----------------------------
DROP TABLE IF EXISTS `channel_to_channel`;
CREATE TABLE `channel_to_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bot_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL COMMENT 'Id канала клиента из таблицы channels',
  `developer_id` int(11) NOT NULL COMMENT 'Id канала разработчика из таблицы channels',
  `moder_id` int(11) NOT NULL COMMENT 'Id канала модератора из таблицы channels',
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`,`developer_id`,`bot_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for dialog
-- ----------------------------
DROP TABLE IF EXISTS `dialog`;
CREATE TABLE `dialog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status1` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for dialog_line
-- ----------------------------
DROP TABLE IF EXISTS `dialog_line`;
CREATE TABLE `dialog_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dialog_id` int(11) NOT NULL,
  `dat_add` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `text` text NOT NULL,
  `format` varchar(30) NOT NULL DEFAULT 'text',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bot_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'Id пользователя-автора сообщения',
  `channel_id` int(11) NOT NULL COMMENT 'Id канала-автора сообщения',
  `moder_id` int(11) DEFAULT NULL COMMENT 'Id канала-модератора для модерации сообщения',
  `mid` int(11) DEFAULT NULL COMMENT 'Id сообщения в чате у автора',
  `mid2` int(11) DEFAULT NULL COMMENT 'Id сообщения в чате у модератора',
  `mid3` int(11) DEFAULT NULL COMMENT 'Id сообщения в чате у получателя',
  `text` text NOT NULL,
  `dat_add` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `format` varchar(30) NOT NULL DEFAULT 'text' COMMENT 'тип сообщения: текст, картинк и т.д.',
  `status1` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'статус сообщения',
  `sent` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'признак отправки (1-да,0-нет)',
  `reply` int(11) DEFAULT NULL COMMENT 'id сообщения в бд, на которое ответили',
  PRIMARY KEY (`id`),
  KEY `bot_id` (`bot_id`),
  KEY `user_id` (`user_id`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for session
-- ----------------------------
DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bot_id` int(11) NOT NULL,
  `chid` varchar(30) NOT NULL,
  `dat_add` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data` text,
  PRIMARY KEY (`id`),
  KEY `bot_id` (`bot_id`),
  CONSTRAINT `session_ibfk_1` FOREIGN KEY (`bot_id`) REFERENCES `bots` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `mid` int(11) DEFAULT NULL,
  `origin_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `format` varchar(30) NOT NULL DEFAULT 'text',
  `status1` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT 'внутретелеграммовский id пользователя',
  `fio` varchar(120) NOT NULL COMMENT 'настоящие имя и фамилия пользователя',
  `nam` varchar(60) DEFAULT NULL COMMENT 'указанное username в телеграме',
  `fak_nam` varchar(60) DEFAULT NULL COMMENT 'фейковые имя и фамилия',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
SET FOREIGN_KEY_CHECKS=1;
