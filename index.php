<?php
declare(strict_types = 1);

define('DIR', __DIR__);
define('LOG_DIR', DIR . '/logs');
define('FILES_DIR', DIR . '/files');

require __DIR__. '/bootstrap.php';

if(file_exists(DIR . '/config/config_loc.php')) {
    include DIR .'/config/config_loc.php';
} else {
    include DIR .'/config/config.php';
}

if(DEBUG_MODE) {
    ini_set('display_errors', "1");
    ini_set('display_startup_errors', "1");

    error_reporting(E_ALL);
    ini_set('log_errors', "1");
    ini_set('error_log', LOG_DIR . '/errors.log');
}



function writeFile($msg, string $file = 'file.txt')
{
   // $msg =  iconv("CP1257","UTF-8", $msg);

    if(is_object($msg) || is_array($msg)){
        file_put_contents(__DIR__.'/logs/'.$file, print_r($msg, true));
    } else {
        file_put_contents(__DIR__.'/logs/'.$file, $msg);
    }
}

function logFile($msg, string $file = 'log.txt')
{
   // $msg = iconv("CP1257","UTF-8", $msg);

    if(is_object($msg) || is_array($msg)) {
        $msg = print_r($msg, true);
    }

    $f = fopen(__DIR__. '/logs/'.$file, 'a+');
    fwrite($f, $msg);
    fwrite($f, PHP_EOL);
    fclose($f);
}

function output($msg)
{
    if(is_object($msg) || is_array($msg)) {
        echo "<pre>";
        print_r($msg);
        echo "</pre>";
    } else {
        echo $msg;
    }
}

function exec_script($url, $params = array())
{
    $parts = parse_url($url);

    if (!$fp = fsockopen($parts['host'], isset($parts['port']) ? $parts['port'] : 80))
    {
        return false;
    }

    $data = http_build_query($params, '', '&');

    fwrite($fp, "POST " . (!empty($parts['path']) ? $parts['path'] : '/') . " HTTP/1.1\r\n");
    fwrite($fp, "Host: " . $parts['host'] . "\r\n");
    fwrite($fp, "Content-Type: application/x-www-form-urlencoded\r\n");
    fwrite($fp, "Content-Length: " . strlen($data) . "\r\n");
    fwrite($fp, "Connection: Close\r\n\r\n");
    fwrite($fp, $data);
    fclose($fp);

    return true;
}

function mb_substr_replace($string, $replacement, $start, $length=NULL) {
    if (is_array($string)) {
        $num = count($string);
        // $replacement
        $replacement = is_array($replacement) ? array_slice($replacement, 0, $num) : array_pad(array($replacement), $num, $replacement);
        // $start
        if (is_array($start)) {
            $start = array_slice($start, 0, $num);
            foreach ($start as $key => $value)
                $start[$key] = is_int($value) ? $value : 0;
        }
        else {
            $start = array_pad(array($start), $num, $start);
        }
        // $length
        if (!isset($length)) {
            $length = array_fill(0, $num, 0);
        }
        elseif (is_array($length)) {
            $length = array_slice($length, 0, $num);
            foreach ($length as $key => $value)
                $length[$key] = isset($value) ? (is_int($value) ? $value : $num) : 0;
        }
        else {
            $length = array_pad(array($length), $num, $length);
        }
        // Recursive call
        return array_map(__FUNCTION__, $string, $replacement, $start, $length);
    }
    preg_match_all('/./us', (string)$string, $smatches);
    preg_match_all('/./us', (string)$replacement, $rmatches);
    if ($length === NULL) $length = mb_strlen($string);
    array_splice($smatches[0], $start, $length, $rmatches[0]);
    return join($smatches[0]);
}