<?php

declare(strict_types = 1);

include __DIR__ . '/index.php';

$bots = \logics\DB::get()->run("SELECT * FROM `bots`");

foreach ($bots as $bot) {
    $api = new \telegram\TelegramApi($bot['token']);

    $result = $api->deleteWebhook();
    $result = $api->setWebhook(API_BOT_HOOK.'?bot='.$bot['id']);
print_r($result);
}